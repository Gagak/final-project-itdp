import 'package:http/http.dart' as http;

class ApiHelper{
  static final baseUrl = 'http://localhost:8080';

  static Future<http.Response> getAllTransaction(String startDate, String endDate) async{
    String endPoint = '/transaksi-detail-by-date/startDate=${startDate}&endDate=${endDate}';
    try{
      return await http.get(Uri.parse(baseUrl + endPoint));
    } on Exception catch(e){
      throw (e);
    }
  }

  static Future<http.Response> getAllTransactionByCategory(String startDate, String endDate, String categoryId) async{
    String endPoint = '/transaksi-detail-by-date/startDate=${startDate}&endDate=${endDate}&categoryId=${categoryId}';
    try{
      return await http.get(Uri.parse(baseUrl + endPoint));
    } on Exception catch(e){
      throw (e);
    }
  }
}