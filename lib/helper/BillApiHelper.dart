import 'package:http/http.dart' as http;

/**
 * Class untuk manggil API Bill Transaction
 */
class BillApiHelper {
  static final baseUrl = 'http://localhost:8080';

  // Untuk memanggil API GET All
  static Future<http.Response> getAllListBillTransaction() async {
    try {
      return await http.get(Uri.parse('$baseUrl/transaksi&Bill'));
    } on Exception catch (error) {
      // Kalau error
      throw error;
    }
  }

  // Untuk memanggil API GET One
  static Future<http.Response> getOneBillTransaction(int id) async {
    try {
      return await http.get(Uri.parse('$baseUrl/transaksi&Bill/$id'));
    } on Exception catch (error) {
      // Kalau error
      throw error;
    }
  }
}
