class ListBillTransaction {
  final int id;
  DateTime transactionDate;
  String merchantName;
  double totalPrice;
  int? totalCategory;
  int? totalItem;
  DateTime? createdAt = DateTime.now();
  int transactionTypeId;
  int? customerId;

  ListBillTransaction({
    required this.id,
    required this.transactionDate,
    required this.merchantName,
    required this.totalPrice,
    this.totalCategory,
    this.totalItem,
    this.createdAt,
    required this.transactionTypeId,
    this.customerId,
  });

  // Convert JSON ke Object
  ListBillTransaction.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        transactionDate = DateTime.parse(json['transactionDate']),
        merchantName = json['merchantName'],
        totalPrice = json['totalPrice'],
        totalItem = json['totalItem'] == null ? 0 : json['totalItem'],
        totalCategory =
            json['totalCategory'] == null ? 0 : json['totalCategory'],
        createdAt = DateTime.parse(json['createdAt']),
        customerId = json['customer_id'] == null ? -1 : json['customer_id'],
        transactionTypeId = json['transaction_type_id'];

  // Convert Object ke JSON
  Map<String, dynamic> toJson() => {
        'transactionDate': transactionDate,
        'merchantName': merchantName,
        'totalPrice': totalPrice,
        'totalItem': totalItem,
        'totalCategory': totalCategory,
        'customer_Id': customerId,
        'transaction_type_id': transactionTypeId
      };
}
