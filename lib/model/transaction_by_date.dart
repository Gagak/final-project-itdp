class TransactionByDate{
  String categoryId;
  String categoryName;
  DateTime? createdAt = DateTime.now();
  int id;
  String merchantName;
  DateTime? modifiedAt;
  int totalPrice;
  DateTime transactionDate;
  
  TransactionByDate({
    required this.categoryId,
    required this.categoryName,
    this.createdAt,
    required this.id,
    required this.merchantName,
    this.modifiedAt,
    required this.totalPrice,
    required this.transactionDate
  });

  TransactionByDate.fromJson(Map<String,dynamic> model):
    categoryId = model['categoryId'],
    categoryName = model['categoryName'],
    id = model['id'],
    merchantName = model['merchantName'],
    createdAt = model['createdAt'] == null
            ? null
            : DateTime.parse(model['createdAt']),
    modifiedAt = model['modifiedAt'] == null
        ? null
        : DateTime.parse(model['modifiedAt']),
    totalPrice = model['totalPrice'],
    transactionDate = DateTime.parse(model['transactionDate']);
}