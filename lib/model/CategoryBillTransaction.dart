class CategoryBillTransaction {
  final int id;
  final String categoryId;
  final String categoryName;

  CategoryBillTransaction({
    required this.id,
    required this.categoryId,
    required this.categoryName,
  });

  // Convert JSON ke Object
  CategoryBillTransaction.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        categoryId = json['categoryId'],
        categoryName = json['categoryName'];

  // Convert Object ke JSON
  Map<String, dynamic> toJson() =>
      {'categoryId': categoryId, 'categoryName': categoryName};
}
