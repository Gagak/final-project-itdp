import 'dart:convert';

import 'package:final_project_itdp/model/transaction_by_date.dart';
import 'package:flutter/foundation.dart';
import '../helper/ApiHelper.dart';
import '../pages/wealth_management/home_page/data_dummy.dart';

extension StringExtension on String {
    String capitalize() {
      return "${this[0].toUpperCase()}${this.substring(1).toLowerCase()}";
    }
}
class ModelTransactionByDate extends ChangeNotifier{
  List<TransactionByDate> currTransactionByDate = [];
  List<TransactionByDate> prevTransactionByDate = [];
  final List<Map<String, Object>> month = bulan;

  List<TransactionByDate> allTransactionByCategory  = [];

  
  getAllTransactionByCategory(String res, String kategoriId){
    String startDate = res.split(" ")[0];
    String endDate = res.split(" ")[1];
    ApiHelper.getAllTransactionByCategory(startDate, endDate, kategoriId).then((value) {
      if(value.statusCode == 200){
        var data = json.decode(value.body);
        allTransactionByCategory = (data['data'] as List).map((e) => TransactionByDate.fromJson(e)).toList();
        notifyListeners();
      }
    });
  }

  

  getAllCurrTransactionByDate(String res){
    String startDate = res.split(" ")[0];
    String endDate = res.split(" ")[1];
    ApiHelper.getAllTransaction(startDate, endDate).then((response) {
      currTransactionByDate = (json.decode(response.body) as List)
          .map((e) => TransactionByDate.fromJson(e))
          .toList();
      notifyListeners();
    }).catchError((error) {
      notifyListeners();
      throw error;
    });
  }

  getAllPrevTransactionByDate(String res){
    String startDate = res.split(" ")[0];
    String endDate = res.split(" ")[1];
    ApiHelper.getAllTransaction(startDate, endDate).then((response) {
      prevTransactionByDate = (json.decode(response.body) as List)
          .map((e) => TransactionByDate.fromJson(e))
          .toList();
      notifyListeners();
    }).catchError((error) {
      notifyListeners();
      throw error;
    });
  }

  get currTransactions => currTransactionByDate;
  get prevTransactions => prevTransactionByDate;
  get allTransactionsByCategory => allTransactionByCategory;
}