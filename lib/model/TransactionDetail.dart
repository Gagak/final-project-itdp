class TransactionDetail {
  final int id;
  String itemName;
  int itemQuantity;
  double price;
  DateTime? createdAt = DateTime.now();
  int? billTransactionId;
  int? transactionSubCategoryId;

  TransactionDetail({
    required this.id,
    required this.itemName,
    required this.itemQuantity,
    required this.price,
    this.createdAt,
    this.billTransactionId,
  });
}
