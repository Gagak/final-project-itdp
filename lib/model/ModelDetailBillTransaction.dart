import 'dart:convert';

import 'package:final_project_itdp/model/DetailBillTransaction.dart';
import 'package:flutter/foundation.dart';

import '../helper/BillApiHelper.dart';

class ModelDetailBillTransaction extends ChangeNotifier {
  DetailBillTransaction selectedDetailBillTransaction = DetailBillTransaction(
      id: 0,
      transactionDate: "transactionDate",
      merchantName: "merchantName",
      totalPrice: 0,
      transactionTypeId: 0,
      categoryByBill: [],
      detailsByCategory: []);

  // Get One Bill Transaction List
  set selectOneBillTransaction(int id) {
    BillApiHelper.getOneBillTransaction(id).then((response) {
      print("LOG: selected = ${selectedDetailBillTransaction}");
      selectedDetailBillTransaction =
          DetailBillTransaction.fromJson(json.decode(response.body));
      notifyListeners();
    }).catchError((error) {
      notifyListeners();
      throw error;
    });
  }

  get selected => selectedDetailBillTransaction;
}
