import 'dart:convert';

import 'package:final_project_itdp/model/CategoryBillTransaction.dart';
import 'package:final_project_itdp/model/TransactionDetail.dart';

import 'ListBillTransaction.dart';

class DetailBillTransaction {
  final int id;
  String transactionDate;
  String merchantName;
  double totalPrice;
  DateTime? createdAt = DateTime.now();
  int transactionTypeId;
  int? customerId;
  List<dynamic> categoryByBill;
  List<dynamic> detailsByCategory;

  DetailBillTransaction({
    required this.id,
    required this.transactionDate,
    required this.merchantName,
    required this.totalPrice,
    this.createdAt,
    required this.transactionTypeId,
    this.customerId,
    required this.categoryByBill,
    required this.detailsByCategory,
  });

  // Convert JSON ke Object
  DetailBillTransaction.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        transactionDate = json['transactionDate'],
        merchantName = json['merchantName'],
        totalPrice = json['totalPrice'],
        createdAt = DateTime.parse(json['createdAt']),
        customerId = json['customer_id'] == null ? -1 : json['customer_id'],
        transactionTypeId = json['transaction_type_id'],
        categoryByBill = json['category_by_bill'],
        detailsByCategory = json['details_by_category_in_bill'];

  // Convert Object ke JSON
  // Map<String, dynamic> toJson() => {
  //       'transactionDate': transactionDate,
  //       'merchantName': merchantName,
  //       'totalPrice': totalPrice,
  //       'customer_Id': customerId,
  //       'transaction_type_id': transactionTypeId,
  //       'category_by_bill': categoryByBill,
  //       'details_by_category_in_bill': detailsByCategory
  //     };
}
