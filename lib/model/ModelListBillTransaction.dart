import 'dart:convert';

import 'package:final_project_itdp/helper/BillApiHelper.dart';
import 'package:final_project_itdp/model/ListBillTransaction.dart';
import 'package:flutter/foundation.dart';

class ModelListBillTransaction extends ChangeNotifier {
  List<ListBillTransaction> listBillTransactions = [];
  late ListBillTransaction selectedListBillTransaction;
  bool isLoading = false;

  // Get All Bill Transaction List
  getAllListBillTransaction() {
    isLoading = true;
    BillApiHelper.getAllListBillTransaction().then((response) {
      Iterable list = json.decode(response.body);
      listBillTransactions =
          list.map((model) => ListBillTransaction.fromJson(model)).toList();
      // print("LOG: ${listBillTransactions}");
      notifyListeners();
      isLoading = false;
    }).catchError((error) {
      isLoading = false;
      notifyListeners();
      throw error;
    });
  }

  get isLoadingData => isLoading;

  get allListBillTransactions => listBillTransactions;

  // Get One Bill Transaction List
  set selectListBillTransactions(int id) {
    BillApiHelper.getOneBillTransaction(id).then((response) {
      selectedListBillTransaction =
          ListBillTransaction.fromJson(json.decode(response.body));
      notifyListeners();
    }).catchError((error) {
      print("LOG: ERROR = " + error);
    });
  }

  get selected => selectedListBillTransaction;
}
