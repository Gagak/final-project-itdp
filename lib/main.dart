import 'package:final_project_itdp/model/ModelDetailBillTransaction.dart';
import 'package:final_project_itdp/model/ModelListBillTransaction.dart';
import 'package:final_project_itdp/model/model_transaction_by_date.dart';
import 'package:final_project_itdp/pages/mega_home.dart';
import 'package:final_project_itdp/pages/wealth_management/bill_scan_detail/bill_scan_detail_page.dart';
import 'package:final_project_itdp/pages/wealth_management/bill_scan_history/billScanHistoryPage.dart';
import 'package:final_project_itdp/pages/wealth_management/bill_scan_result/DataDummy.dart';
import 'package:final_project_itdp/pages/wealth_management/bill_scan_result/ScanBill.dart';
import 'package:final_project_itdp/pages/wealth_management/bill_scan_result/Scan_Bill/Modal_Edit.dart';
import 'package:final_project_itdp/pages/wealth_management/expense_history_by_category/expenseHistoryPage.dart';
import 'package:final_project_itdp/pages/wealth_management/expense_detail/RincianTransaksi.dart';
import 'package:final_project_itdp/pages/wealth_management/bill_scan_page/error_page.dart';
import 'package:final_project_itdp/pages/wealth_management/home_page/home_page.dart';
import 'package:final_project_itdp/pages/wealth_management/home_page/modal_home_page/periodic_modal.dart';
import 'package:final_project_itdp/pages/wealth_management/portal_ojk/portalOJKPage.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'pages/wealth_management/bill_scan_page/bill_scan_page.dart';
import 'pages/wealth_management/wishlist/wishlist_home/wishlistHomePage.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => ModelListBillTransaction()),
        ChangeNotifierProvider(
            create: (context) => ModelDetailBillTransaction()),
        ChangeNotifierProvider(create: (context) => ModelTransactionByDate()),
        ChangeNotifierProvider(
          create: (context) => DataDummy(),
        )
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        initialRoute: '/',
        routes: {
          //@follow-up main-routing
          '/': (context) => const MegaHomeScreen(),

          // Dashboard
          '/wealth': (context) => HomePage(),

          // Scan Bill
          '/scan_bill': (context) => ScanPage(),

          // Scan Bill Error
          '/error': (context) => const ScanError(),

          // Scan Bill Detail Input
          '/detail_scan': (context) => const ScanBill(),

          // Detail Pengeluaran
          '/transaksi_history': (context) => ExpenseHistoryPage(),

          // Rincian Transaksi
          '/rincian': (context) => const RincianTransaksi(),

          // Bill History
          '/bill_history': (context) => const BillScanHistoryPage(),

          // Bill History Details
          '/bill_history_details': (context) => BillScanDetail(),

          // Portal OJK
          '/portal_ojk': (context) => const PortalOJKPage(),

          // Wishlist
          '/wishlist': (context) => const WishlistHomePage(),
        },
      ),
    );
  }
}
