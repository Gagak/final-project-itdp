import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

class MegaHomeScreen extends StatelessWidget {
  const MegaHomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[100]!,
      body: SingleChildScrollView(
        child: Column(children: [
          Image.asset(
            'assets/images/header.png',
            fit: BoxFit.fitHeight,
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                  margin:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: const [
                      Text(
                        "Hai, Selamat Datang",
                        style: TextStyle(fontSize: 20, color: Colors.grey),
                      ),
                      Text(
                        'Haikal Ravendy Yusmananda',
                        style: TextStyle(fontSize: 15),
                      ),
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    MyButton(
                      icon: Icons.qr_code,
                      color: Colors.purple[400]!,
                      text: 'QRIS',
                      routing: 'qris',
                    ),
                    MyButton(
                      icon: Icons.add_card,
                      color: Colors.blue[800]!,
                      text: 'Top Up',
                      routing: 'top_up',
                    ),
                    MyButton(
                      icon: Icons.payments_outlined,
                      color: Colors.green,
                      text: 'Bayar',
                      routing: 'bayar',
                    ),
                    MyButton(
                      icon: Icons.exit_to_app,
                      color: Colors.blue[400]!,
                      text: 'Transfer',
                      routing: 'transfer',
                    )
                  ],
                ),
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.symmetric(vertical: 10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Colors.white,
            ),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    MyButton(
                      icon: Icons.catching_pokemon,
                      color: Colors.orange[600]!,
                      text: 'Tarik Tunai',
                      routing: 'tarik_tunai',
                    ),
                    MyButton(
                      icon: Icons.account_box,
                      color: Colors.orange[600]!,
                      text: 'Akun Saya',
                      routing: 'akun_saya',
                    ),
                    MyButton(
                      icon: Icons.medical_services_rounded,
                      color: Colors.orange[600]!,
                      text: 'Layanan',
                      routing: 'layanan',
                    ),
                    MyButton(
                      icon: Icons.insert_chart,
                      color: Colors.orange[600]!,
                      text: 'Investasi',
                      routing: 'investasi',
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 15.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      MyButton(
                        icon: Icons.edit_note_sharp,
                        color: Colors.orange[600]!,
                        text: 'Apply',
                        routing: 'apply',
                      ),
                      MyButton(
                        icon: Icons.info,
                        color: Colors.orange[600]!,
                        text: 'Info Lainnya',
                        routing: 'other_info',
                      ),
                      MyButton(
                        icon: Icons.attach_money,
                        color: Colors.orange[600]!,
                        text: 'Smart Financial',
                        routing: 'wealth',
                      ),
                      MyButton(
                        icon: Icons.grid_view_rounded,
                        color: Colors.orange[600]!,
                        text: 'Lainnya',
                        routing: 'lainnya',
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: const [
                      Text(
                        'HighLights',
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                      Text('Lihat Semuanya',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                              color: Colors.orange))
                    ],
                  ),
                ),
                CarouselSlider(
                  options: CarouselOptions(
                      height: 200.0,
                      autoPlay: true,
                      enableInfiniteScroll: false,
                      enlargeCenterPage: true),
                  items: [1, 2, 3, 4, 5].map((i) {
                    return Builder(
                      builder: (BuildContext context) {
                        return Container(
                            width: MediaQuery.of(context).size.width,
                            margin: const EdgeInsets.symmetric(horizontal: 5.0),
                            decoration:
                                const BoxDecoration(color: Colors.amber),
                            child: Text(
                              'text $i',
                              style: const TextStyle(fontSize: 16.0),
                            ));
                      },
                    );
                  }).toList(),
                )
              ],
            ),
          ),
        ]),
      ),
    );
  }
}

class MyButton extends StatelessWidget {
  IconData icon;
  Color color;
  String text;
  String routing;
  MyButton({
    super.key,
    required this.icon,
    required this.color,
    required this.text,
    required this.routing,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ElevatedButton(
          onPressed: () {
            Navigator.pushNamed(context, "/$routing");
          },
          style: ElevatedButton.styleFrom(
            backgroundColor: color,
            fixedSize: const Size(60, 60),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15),
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(
                icon,
                size: 30,
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 10),
          child: Text(
            text,
            style: const TextStyle(color: Colors.grey),
          ),
        )
      ],
    );
  }
}
