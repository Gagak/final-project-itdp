import 'dart:ui';
import 'package:flutter/material.dart';

const warnaAppBar = Color(0xFFFDC60C);
const warnaBox = Color(0xFFD9D9D9);
const warnaTextPrimary = Colors.black;
const warnaTextOrange = Color(0xFF0F9E0C);
const warnaTextSubTitle = Color(0xFF4E4E4E);
const warnaTextModerateOpacity = Color(0xFFC2BEBE);
const warnaTextLowOpacity = Color(0xFFD9D9D9);
const warnaBackButton = Color(0xFFD9D9D9);
const warnaBookCardButton = Color(0xFFEA6E48);
const backButtonStyle = ButtonStyle(
  backgroundColor: MaterialStatePropertyAll(
    Color(0xFFD9D9D9),
  ),
);
