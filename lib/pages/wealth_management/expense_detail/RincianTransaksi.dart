import 'package:final_project_itdp/pages/wealth_management/expense_detail/BoxCategory.dart';
import 'package:final_project_itdp/pages/wealth_management/expense_detail/CardTrx.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

import '../constants.dart';

// ignore: todo
//TODO: Halaman utama Rincian Transaksi
class RincianTransaksi extends StatelessWidget {
  const RincianTransaksi({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 80.0,
        leading: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: const EdgeInsets.all(13.0),
              height: 25,
              width: 25,
              child: TextButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                style: backButtonStyle,
                child: const Text("<"),
              ),
            ),
          ],
        ),
        backgroundColor: warnaAppBar,
        elevation: 0.0,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(15),
          ),
        ),
        title: const Text(
          "Rincian Transaksi",
          style: TextStyle(
              fontSize: 18,
              color: warnaTextPrimary,
              fontWeight: FontWeight.w700),
        ),
        centerTitle: true,
      ),
      backgroundColor: const Color(0xFFFDC60C),
      body: Stack(children: [
        Column(
          children: [
            Container(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    height: 80,
                  ),
                ],
              ),
            ),
            Expanded(
              child: Container(
                margin: const EdgeInsets.only(top: 20),
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20),
                  ),
                ),
              ),
            ),
          ],
        ),
        Column(
          children: [
            //bisa diganti Buat naro card atau bisa container yg isinya logo jumlah id dll
            Expanded(
              flex: 1,
              // child: CardTrx(),
              child: Container(
                padding: const EdgeInsets.all(10),
                margin: const EdgeInsets.fromLTRB(35, 40, 35, 0),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.black.withOpacity(0.05),
                          spreadRadius: 5,
                          blurRadius: 5)
                    ]),
                child: const CardTrx(),
              ),
            ),

            Expanded(
              flex: 1,
              child: Column(
                children: const [
                  BoxCategory(),
                ],
              ),
            ),
          ],
        ),
      ]),
    );
  }
}
