import 'package:final_project_itdp/pages/wealth_management/expense_detail/ModalCategory.dart';
import 'package:flutter/material.dart';

class BoxCategory extends StatefulWidget {
  const BoxCategory({super.key});

  @override
  State<BoxCategory> createState() => _BoxCategoryState();
}

class _BoxCategoryState extends State<BoxCategory> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.fromLTRB(20, 20, 20, 0),
      decoration: BoxDecoration(
          color: const Color.fromARGB(255, 248, 248, 248),
          boxShadow: [
            BoxShadow(
                color: Colors.black.withOpacity(0.05),
                spreadRadius: 1,
                blurRadius: 1,
                offset: const Offset(0, 2))
          ]),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: const [
              Image(
                image: AssetImage('images/rincian_transaksi/Belanja.png'),
                height: 30,
                color: Colors.amber,
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                'Belanja',
                style: TextStyle(fontWeight: FontWeight.w600),
              ),
            ],
          ),
          //Tombol Buat Modal
          ElevatedButton(
              style: ElevatedButton.styleFrom(
                  side: BorderSide(
                      color: Colors.black.withOpacity(0.5), width: 1),
                  backgroundColor: Colors.white),
              onPressed: () => MyModal.ChooseCategoryModal(context),
              child: const Text(
                'Ubah Kategori',
                style: TextStyle(color: Colors.black),
              ))
        ],
      ),
    );
  }
}
