import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

class CardExcluded extends StatelessWidget {
  const CardExcluded({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        // border: Border.all(color: Colors.black, width: 2.0),
        borderRadius: BorderRadius.circular(10),
      ),
      padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
      alignment: Alignment.center,
      child: const Padding(
        padding: EdgeInsets.all(10.0),
        child: Text(
          'Dengan Memilih Kategori "Excluded" maka pengeluaran ini tidak akan dimasukkan ke dalam perhitungan analisis pengeluaran dan grafik akan berubah. \n\n Apakah anda yakin untuk memilih kategori "Excluded"?',
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 16),
        ),
      ),
    );
  }
}
