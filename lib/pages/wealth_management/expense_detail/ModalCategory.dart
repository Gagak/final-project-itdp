import 'package:final_project_itdp/pages/wealth_management/expense_detail/CardExcluded.dart';
import 'package:flutter/material.dart';

const List<String> listMainCategory = <String>['Excluded', 'Belanja', 'Games'];
const List<String> listSubCategory = <String>['-', 'Fashion', 'Games'];
const Map<String, List<String>> subCategory = {
  'Excluded': ['Null'],
  'Belanja': ['Fashion', 'Ikan Cupang'],
  'Games': ['Mobile Legend', 'Free Fire', 'Genshin']
};
var isExcluded = true;

class MyModal {
  static Future<void> ChooseCategoryModal(BuildContext context) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Center(child: Text('Pilih Kategori')),
          titlePadding: const EdgeInsets.symmetric(vertical: 20),
          content: Container(
            height: 220,
            width: MediaQuery.of(context).size.width,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                ChooseCategoryDropDown(),
                Column(
                  children: [
                    TextButton(
                      style: TextButton.styleFrom(
                          backgroundColor: Colors.white,
                          foregroundColor: Colors.white),
                      child: const Text(
                        'OKE',
                        style: TextStyle(
                            color: Color(0xFFF39823),
                            fontWeight: FontWeight.bold),
                      ),
                      onPressed: () {
                        Navigator.of(context).pop();
                        if (isExcluded) {
                          AlertBox(context);
                        }
                      },
                    ),
                    Container(
                      width: 30,
                      height: 3,
                      color: const Color(0XFF999B9E),
                    )
                  ],
                ),
              ],
            ),
          ),
          actions: <Widget>[],
        );
      },
    );
  }

  static Future<void> AlertBox(BuildContext context) {
    return showDialog<void>(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            actionsPadding: const EdgeInsets.all(0),
            contentPadding:
                const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
            content: const CardExcluded(),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadiusDirectional.circular(10)),
            actions: <Widget>[
              Row(
                // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                      child: SizedBox(
                    height: 40,
                    child: TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                        ChooseCategoryModal(context);
                      },
                      style: const ButtonStyle(
                          backgroundColor:
                              MaterialStatePropertyAll(Color(0xFFD8D0D0)),
                          shape: MaterialStatePropertyAll(
                              RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      bottomLeft: Radius.circular(10))))),
                      child: const Text(
                        'Tidak',
                        style: TextStyle(color: Colors.black),
                      ),
                    ),
                  )),
                  Expanded(
                      child: SizedBox(
                    height: 40,
                    child: TextButton(
                      onPressed: () {},
                      style: const ButtonStyle(
                          backgroundColor:
                              MaterialStatePropertyAll(Color(0xFFFDC60C)),
                          shape: MaterialStatePropertyAll(
                              RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      bottomRight: Radius.circular(10))))),
                      child: const Text(
                        'Ya',
                        style: TextStyle(color: Colors.black),
                      ),
                    ),
                  ))
                ],
              )
            ],
          );
        });
  }
}

class ChooseCategoryDropDown extends StatefulWidget {
  ChooseCategoryDropDown({super.key});

  @override
  State<ChooseCategoryDropDown> createState() => _ChooseCategoryDropDownState();
}

class _ChooseCategoryDropDownState extends State<ChooseCategoryDropDown> {
  String? subDropDownValue;
  String? mainDropDownValue;
  List<String> mySubCategory = ['Null'];
  String firstSubCategory = 'Excluded';

  toDropDownItem(List<String> list) {
    return list.map<DropdownMenuItem<String>>((String value) {
      return DropdownMenuItem<String>(
        value: value,
        child: Text(value),
      );
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Text('Kategori'),
        const SizedBox(
          height: 5,
        ),
        Container(
          decoration: BoxDecoration(
              border: Border.all(color: Colors.black, width: 2.0),
              borderRadius: BorderRadius.circular(10)),
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Center(
              child: DropdownButton<String>(
            value: mainDropDownValue ?? listMainCategory.first,
            icon: const Icon(Icons.keyboard_arrow_down_sharp),
            elevation: 16,
            underline: const SizedBox(),
            onChanged: (String? value) {
              // This is called when the user selects an item.
              print(value);
              setState(() {
                if (value == 'Excluded') {
                  isExcluded = true;
                } else {
                  isExcluded = false;
                }
                mainDropDownValue = value!;
                mySubCategory = subCategory[mainDropDownValue]!;
                subDropDownValue = subCategory[mainDropDownValue]!.first;
              });
            },
            isExpanded: true,
            items:
                listMainCategory.map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value),
              );
            }).toList(),
          )),
        ),
        const SizedBox(
          height: 10,
        ),
        const Text('Sub Kategori'),
        const SizedBox(
          height: 5,
        ),
        Container(
          decoration: BoxDecoration(
              border: Border.all(color: Colors.black, width: 2.0),
              borderRadius: BorderRadius.circular(10)),
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Center(
              child: DropdownButton<String>(
            value: subDropDownValue ?? subCategory[firstSubCategory]!.first,
            icon: const Icon(Icons.keyboard_arrow_down_sharp),
            elevation: 16,
            underline: const SizedBox(),
            onChanged: (String? value) {
              if (mySubCategory.first == 'Null') {
                null;
              } else {
                setState(() {
                  subDropDownValue = value!;
                });
              }
              // This is called when the user selects an item.
            },
            isExpanded: true,
            items: mySubCategory.first == 'Null'
                ? []
                : toDropDownItem(mySubCategory),
          )),
        ),
        const SizedBox(
          height: 20,
        ),
      ],
    );
  }
}
