import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../constants.dart';

class EBookCard extends StatelessWidget {
  String imageUrl;
  String title;
  String subtitle;
  String bookLink;
  String? download;
  EBookCard({
    required this.imageUrl,
    required this.title,
    required this.subtitle,
    required this.bookLink,
    this.download,
  });

  @override
  Widget build(BuildContext context) {
    final Uri bookUri = Uri.parse(bookLink);
    final Uri downloadUri = Uri.parse(download!);

    return Stack(
      children: <Widget>[
        Container(
          margin: const EdgeInsets.all(10.0),
          padding: const EdgeInsets.only(left: 100, right: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(11),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(1),
                spreadRadius: 1,
                blurRadius: 5,
                offset: Offset(0, 2),
              ),
            ],
            color: Colors.white,
          ),
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // Row subtitle
                Text(
                  "${subtitle}",
                  style: const TextStyle(
                    color: warnaTextSubTitle,
                    fontSize: 14.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),

                // Row title
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 5.0),
                  child: Text(
                    "${title}",
                    overflow: TextOverflow.clip,
                    style: const TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),

                // Row Button
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 0.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      ElevatedButton.icon(
                        onPressed: () => launchBookUrl(bookUri),
                        icon: const Icon(Icons.book_rounded),
                        label: const Text("Baca"),
                        style: ElevatedButton.styleFrom(
                          backgroundColor: warnaBookCardButton,
                        ),
                      ),
                      ElevatedButton.icon(
                        onPressed: () => launchBookUrl(downloadUri),
                        icon: const Icon(Icons.download_rounded),
                        label: const Text("Unduh"),
                        style: ElevatedButton.styleFrom(
                          backgroundColor: warnaBookCardButton,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),

        // Gambar
        Positioned(
          top: 0,
          left: 0,
          child: Container(
            width: 100,
            height: 140,
            color: Colors.green,
            child: Image(
              image: NetworkImage("${imageUrl}"),
              width: 100,
              height: 140,
            ),
          ),
        ),
      ],
    );
  }

  Future<void> launchBookUrl(Uri link) async {
    if (!await launchUrl(link)) {
      throw 'Could not launch $link';
    }
  }
}
