import 'package:final_project_itdp/pages/wealth_management/portal_ojk/widgets/eBookCard.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

import '../constants.dart';

class PortalOJKPage extends StatefulWidget {
  const PortalOJKPage({super.key});

  @override
  State<PortalOJKPage> createState() => _PortalOJKPageState();
}

class _PortalOJKPageState extends State<PortalOJKPage> {
  @override
  Widget build(BuildContext context) {
    List<String> imageUrl = [
      "https://cdn.pixabay.com/index/2022/11/03/15-54-56-290_1440x550.jpg",
      "https://cdn.pixabay.com/index/2022/11/03/15-54-56-290_1440x550.jpg",
    ];
    List<String> subtitle = [
      "BUKU I",
      "BUKU II",
    ];
    List<String> title = [
      "OJK dan Pengawasan Mikroprudensial",
      "Perbankan",
    ];
    List<String> bookLink = [
      "https://sikapiuangmu.ojk.go.id/FrontEnd/LiterasiPerguruanTinggi/book/book1/reader.html",
      "https://sikapiuangmu.ojk.go.id/FrontEnd/LiterasiPerguruanTinggi/book/book2/reader.html",
    ];
    List<String> download = [
      "https://sikapiuangmu.ojk.go.id/FrontEnd/LiterasiPerguruanTinggi/assets/pdf/Buku%201%20-%20OJK%20dan%20Pengawasan%20Mikroprudensial.pdf",
      "https://sikapiuangmu.ojk.go.id/FrontEnd/LiterasiPerguruanTinggi/assets/pdf/Buku%202%20-%20Perbankan.pdf",
    ];

    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 100.0,
        leading: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: const EdgeInsets.all(13.0),
              height: 25,
              width: 25,
              child: TextButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                style: backButtonStyle,
                child: const Text("<"),
              ),
            ),
          ],
        ),
        backgroundColor: warnaAppBar,
        elevation: 0.0,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(15),
          ),
        ),
        title: const Text(
          "Literature Keuangan",
          style: TextStyle(
              fontSize: 18,
              color: warnaTextPrimary,
              fontWeight: FontWeight.w700),
        ),
        centerTitle: true,
      ),
      body: Column(
        children: [
          Expanded(
            child: Container(
              margin: const EdgeInsets.only(top: 30.0, left: 10.0, right: 10.0),
              child: Column(
                children: [
                  Flexible(
                    child: ListView.builder(
                      itemCount: title.length,
                      itemBuilder: (context, index) {
                        return EBookCard(
                          imageUrl: imageUrl[index],
                          title: title[index],
                          subtitle: subtitle[index],
                          bookLink: bookLink[index],
                          download: download[index],
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
