import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_rounded_progress_bar/flutter_rounded_progress_bar.dart';
import 'package:flutter_rounded_progress_bar/rounded_progress_bar_style.dart';

// void main() => runApp(MyApp());

class ScanPage extends StatefulWidget {
  @override
  _ScanPageState createState() => _ScanPageState();
}

class _ScanPageState extends State<ScanPage> {
  double percent = 10;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      while (percent <= 100) {
        await Future.delayed(Duration(milliseconds: 500), () {
          setState(() {
            print(percent);
            percent += 5;
            if (percent == 100) {
              Navigator.pushNamed(context, '/error');
            }
          });
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Colors.orange,
          elevation: 0.0,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.vertical(bottom: Radius.circular(15))),
          bottom: PreferredSize(
            child: Padding(
              padding: const EdgeInsets.only(left: 25, right: 25, bottom: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    height: 25,
                    width: 25,
                    child: TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      style: ButtonStyle(
                          backgroundColor:
                              MaterialStatePropertyAll(Color(0xFFD9D9D9))),
                      child: const Text("<"),
                    ),
                  ),
                  Text(
                    "Scan Bill",
                    style: TextStyle(
                        fontSize: 18,
                        color: Colors.black,
                        fontWeight: FontWeight.w700),
                  ),
                  Container(
                    height: 25,
                    width: 25,
                  ),
                ],
              ),
            ),
            preferredSize: Size.fromHeight(50),
          ),
        ),
        body: Center(
          child: ListView(children: <Widget>[
            Column(
              children: <Widget>[
                SizedBox(
                  height: 70,
                ),
                Image(image: AssetImage('assets/images/Scan.png')),
                SizedBox(
                  height: 30,
                ),
                SizedBox(
                  width: 400,
                  height: 45,
                  child: RoundedProgressBar(
                      style: RoundedProgressBarStyle(
                        backgroundProgress: Colors.orange,
                        colorProgressDark: Colors.orange,
                        colorProgress: Colors.white,
                        colorBorder: Colors.orange,
                      ),
                      height: 30,
                      milliseconds: 1000,
                      percent: percent,
                      // theme: RoundedProgressBarTheme.yellow,
                      borderRadius: BorderRadius.circular(24)),
                ),
                SizedBox(
                  height: 30,
                ),
                Text('Sistem sedang membaca'),
                Text('Tunggu sebentar'),
                SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.red,
                    minimumSize: Size(150, 48),
                  ),
                  child: Text('Batal'),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ],
            )
          ]),
        ),
      ),
    );
  }
}
