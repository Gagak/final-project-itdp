import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/basic.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

class ScanError extends StatelessWidget {
  const ScanError({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          height: 400,
          width: 300,
          decoration: BoxDecoration(
            color: Color.fromARGB(255, 255, 255, 255),
            borderRadius: BorderRadius.circular(10),
          ),
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(height: 20),
              Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                Image.asset('assets/images/Error.png'),
                Image.asset('assets/images/Error_circle.png'),
              ]),
              SizedBox(height: 20),
              Text(
                'Proses Gagal',
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    color: Color.fromARGB(255, 13, 18, 77)),
              ),
              SizedBox(height: 20),
              Column(
                children: [
                  Text(
                    'Maaf Sistem tidak bisa membaca bill, silahkan',
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        color: Color.fromARGB(255, 13, 18, 77)),
                  ),
                  Text(
                    'input manual atau coba lagi',
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        color: Color.fromARGB(255, 13, 18, 77)),
                  ),
                ],
              ),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Image.asset('assets/images/Oops.png'),
                ],
              ),
              Row(
                children: [
                  ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        fixedSize: Size(150, 50),
                        primary: Color.fromARGB(255, 255, 48, 48),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(10)),
                        ),
                      ),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text("Keluar")),
                  ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        fixedSize: Size(150, 50),
                        primary: Color.fromARGB(255, 38, 211, 128),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                              bottomRight: Radius.circular(10)),
                        ),
                      ),
                      onPressed: () {
                        Navigator.pushNamed(context, "/detail_scan");
                      },
                      child: Text("Input Manual")),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
