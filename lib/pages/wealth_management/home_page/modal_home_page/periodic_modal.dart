import 'package:buttons_tabbar/buttons_tabbar.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:final_project_itdp/pages/wealth_management/home_page/home_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_custom_dialog/flutter_custom_dialog.dart';

import '../data_dummy.dart';

final List<String> months = [
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember',
];

final List<String> years = Selection.listYear;

extension StringExtension on String {
    String capitalize() {
      return "${this[0].toUpperCase()}${this.substring(1).toLowerCase()}";
    }
}

 class function{
  final dataDummy = dummy;
  final List<Map<String, Object>> month = bulan;

  getDiff(){
    int dif;
    if(Selection.selectedTab==0){
      dif = 0;
    }
    else if(Selection.selectedTab==1){
      dif = 3;
    }
    else if(Selection.selectedTab==2){
      dif = 6;
    }
    else{
      dif = 12;
    }
    return dif;
  }

  getPreviousMonthBasedOnDifferent(){
    int dif = getDiff();
    int currMonth = month.indexWhere((element) => element['name'] == Selection.currMonth);
    int prevMonth = currMonth-dif;
    int prevYear = Selection.currYear;
    if(prevMonth<0){
      prevMonth = 12+prevMonth;
      prevYear = Selection.currYear-1;
    }

    return {
      'month': month[prevMonth]['name'],
      'year': prevYear
    };
  }
 }


class DropDown1 extends StatefulWidget {
  DropDown1({super.key});

  @override
  State<DropDown1> createState() => _DropDown1State();
}

class _DropDown1State extends State<DropDown1> {
  String val = years[0];
  
  
  @override
  Widget build(BuildContext context) {
    Selection.selectedTab = 0;
    if(Selection.tabController.index==0){
      Selection.nextMonth = Selection.currMonth;
      Selection.nextYear = Selection.currYear; 
    }
    
    return Row(
      children: [
        DropdownButton(
      // isExpanded: true,
          hint: Text("Pilih Bulan"),
          underline: Container(
            height: 2,
            color: Colors.black,
          ),
          icon: Visibility(visible: false, child: Icon(Icons.arrow_downward)),
          items: months
              .map((item) => DropdownMenuItem<String>(
                    value: item,
                    child: Text(
                      item,
                      style: const TextStyle(
                        fontSize: 14,
                        color: Colors.black,
                      ),
                      overflow: TextOverflow.ellipsis,
                    ),
                  ))
              .toList(),
          value: Selection.currMonth.capitalize().isEmpty?null:Selection.currMonth.capitalize(),
          onChanged: (value) {
            setState(() {
              Selection.currMonth = value as String;
              Selection.nextMonth = value as String;
              Selection.prevNextMonth = Selection.currMonth;
              Selection.prevCurrMonth = function().getPreviousMonthBasedOnDifferent()['month'];

            });
          },
        ),

        SizedBox(
          width: 50,
        ),

        DropdownButton(
          underline: Container(
            height: 2,
            color: Colors.black,
          ),
          icon: Visibility(visible: false, child: Icon(Icons.arrow_downward)),
          items: years
              .map((item) => DropdownMenuItem<String>(
                    
                    value: item,
                    enabled: item == val? false:true,
                    child: Text(
                      item,
                      style: TextStyle(
                        fontSize: 14,
                        color: item == val ?Colors.grey:Colors.black,
                      ),
                      overflow: TextOverflow.ellipsis,
                    ),
                  ))
              .toList(),
          hint: Text("Pilih Tahun"),
          value: Selection.currYear == -1 ? null : Selection.currYear.toString(),
          onChanged: (value) {
            setState(() {
              
              Selection.currYear = int.parse(value??'0');
              Selection.nextYear = int.parse(value??'0');
              Selection.prevNextYear = Selection.currYear;
              Selection.prevCurrYear = function().getPreviousMonthBasedOnDifferent()['year'];
              
            });
          })
      ],
    );
  }
}

class DropdDown3 extends StatefulWidget {
  const DropdDown3({super.key});

  @override
  State<DropdDown3> createState() => _DropdDown3State();
}

class _DropdDown3State extends State<DropdDown3> {
  String val = years[0];
  
  @override
  Widget build(BuildContext context) {
    if(Selection.tabController.index==1){
      int idxMonth = months.indexOf(Selection.currMonth.capitalize());
      if(idxMonth >9){
        int newIndex = idxMonth - 10;
        Selection.nextMonth = months[newIndex];
        Selection.nextYear = Selection.currYear + 1;

      }
      else{
        Selection.nextMonth = months[idxMonth+2];
        Selection.nextYear = Selection.currYear;
      }
    }
    
    
    return Column(
      children: [
        
        Row(
          children: [
            Text('Dari'),
            SizedBox(
              width: 50,
            ),
            DropdownButton(
          // isExpanded: true,
              hint: Text("Pilih Bulan"),
              underline: Container(
                height: 2,
                color: Colors.black,
              ),
              icon: Visibility(visible: false, child: Icon(Icons.arrow_downward)),
              items: months
                  .map((item) => DropdownMenuItem<String>(
                        value: item,
                        child: Text(
                          item,
                          style: const TextStyle(
                            fontSize: 14,
                            color: Colors.black,
                          ),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ))
                  .toList(),
              value: (Selection.currMonth.capitalize().isEmpty?null:Selection.currMonth.capitalize()),
              onChanged: (value) {
                int index = months.indexOf(value as String);
                setState(() {
                  Selection.selectedTab = 1;
                  Selection.currMonth = value as String;
                  Selection.prevNextMonth = Selection.currMonth;
                  months.asMap().forEach((index, element) {
                    if (element == Selection.currMonth) {
                      
                      if (index > 9) {
                        int newindex = (index + 2) - 12;
                        Selection.nextMonth = months[newindex];
                        Selection.nextYear = Selection.currYear + 1;
                        Selection.prevCurrMonth = function().getPreviousMonthBasedOnDifferent()['month'];
                        Selection.prevCurrYear = function().getPreviousMonthBasedOnDifferent()['year'];
                        
                        
                      } else {
                        Selection.nextMonth = months[index + 2];
                        Selection.prevCurrMonth = function().getPreviousMonthBasedOnDifferent()['month'];
                        Selection.prevCurrYear = function().getPreviousMonthBasedOnDifferent()['year'];
                      }
                    }
                  });
                });
              },
            ),
            SizedBox(
              width: 50,
            ),
            DropdownButton(
            key: UniqueKey(),
            underline: Container(
              height: 2,
              color: Colors.black,
            ),
            icon: Visibility(visible: false, child: Icon(Icons.arrow_downward)),
            items: years
                .map((item){ 
                  if(item==val){
                    
                  }
                  return DropdownMenuItem<String>(
                      value: item,
                      enabled: item == val? false:true,
                      child: Text(
                        item,
                        style: TextStyle(
                          fontSize: 14,
                          color: item == val ?Colors.grey:Colors.black,
                        ),
                        overflow: TextOverflow.ellipsis,
                      ),
                    );
                  }
                )
                .toList(),
            hint: Text("Pilih Tahun"),
            value: (Selection.currYear == -1 ? null : Selection.currYear.toString()),
            onChanged: (value) {
              setState(() {
                Selection.currYear = int.parse(value??"0");
                Selection.prevNextYear = Selection.currYear;
                int indexMonth = months.indexOf(Selection.currYear.toString());
                years.asMap().forEach((index, element) {
                  if (element == value.toString()) {
                    if (indexMonth > 9) {
                      int newYear = Selection.currYear + 1;
                      Selection.nextYear = newYear;
                    } else {
                      Selection.nextYear = Selection.currYear;
                    }
                  }
                });
              });
            })
          ],
        ),

        Row(
          children: [
            Text('Sampai'),
            SizedBox(
              width: 25,
            ),
            IgnorePointer(
              ignoring: true,
              child: DropdownButton(
                  // isExpanded: true,
                  hint: Text("Pilih Bulan"),
                  underline: Container(
                    height: 2,
                    color: Colors.black,
                  ),
                  icon: Visibility(visible: false, child: Icon(Icons.arrow_downward)),
                  items: months
                      .map((item) => DropdownMenuItem<String>(
                            value: item,
                            child: Text(
                              item,
                              style: const TextStyle(
                                fontSize: 14,
                                color: Colors.grey,
                              ),
                              overflow: TextOverflow.ellipsis,
                            ),
                          ))
                      .toList(),
                  value: (Selection.nextMonth.capitalize().isEmpty?null:Selection.nextMonth.capitalize()),
                  onChanged: (value) {
                    setState(() {
                    });
                  }),
            ),
            SizedBox(
              width: 50,
            ),
            IgnorePointer(
              ignoring: true,
              child: DropdownButton(
                  underline: Container(
                    height: 2,
                    color: Colors.black,
                  ),
                  icon: Visibility(visible: false, child: Icon(Icons.arrow_downward)),
                  items: years
                      .map((item) => DropdownMenuItem<String>(
                            value: item,
                            child: Text(
                              item,
                              style: const TextStyle(
                                fontSize: 14,
                                color: Colors.grey,
                              ),
                              overflow: TextOverflow.ellipsis,
                            ),
                          ))
                      .toList(),
                  hint: Text("Pilih Tahun"),
                  value: (Selection.nextYear == -1 ? null : Selection.nextYear.toString()),
                  onChanged: (value) {
                    setState(() {
                    });
                  }),
            )
          ],
        )
      ],
    );
  }
}

class DropDown6 extends StatefulWidget {
  const DropDown6({super.key});

  @override
  State<DropDown6> createState() => _DropDown6State();
}

class _DropDown6State extends State<DropDown6> {
  String val = years[0];
  @override
  Widget build(BuildContext context) {
    if(Selection.tabController.index==2){
      int idxMonth = months.indexOf(Selection.currMonth.capitalize());
      if(idxMonth >6){
        int newIndex = idxMonth - 7;
        Selection.nextMonth = months[newIndex];
        Selection.nextYear = Selection.currYear + 1;
      }
      else{
        Selection.nextMonth = months[idxMonth+5];
        Selection.nextYear = Selection.currYear;
      }
    }

    return Column(
      children: [
        
        Row(
          children: [
            Text('Dari'),
            SizedBox(
              width: 50,
            ),
            DropdownButton(
          // isExpanded: true,
              hint: Text("Pilih Bulan"),
              underline: Container(
                height: 2,
                color: Colors.black,
              ),
              icon: Visibility(visible: false, child: Icon(Icons.arrow_downward)),
              items: months
                  .map((item) => DropdownMenuItem<String>(
                        value: item,
                        child: Text(
                          item,
                          style: const TextStyle(
                            fontSize: 14,
                            color: Colors.black,
                          ),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ))
                  .toList(),
              value: (Selection.currMonth.isEmpty
                  ? null
                  : Selection.currMonth.capitalize()),
              onChanged: (value) {
                int newindex = 0;
                setState(() {
                  Selection.selectedTab=2;
                  Selection.currMonth = value as String;
                  Selection.prevNextMonth = Selection.currMonth;
                  months.asMap().forEach((index, element) {
                    if (element == Selection.currMonth) {
                      
                      if (index > 6) {
                        int newindex = (index + 5) - 12;
                        
                        
                        Selection.nextMonth = months.elementAt(newindex);
                        Selection.nextYear = Selection.currYear+1;
                        Selection.prevCurrMonth = function().getPreviousMonthBasedOnDifferent()['month'];
                        Selection.prevCurrYear = function().getPreviousMonthBasedOnDifferent()['year'];
                        
                      } else {
                        Selection.nextMonth = months.elementAt(index + 5);
                        Selection.prevCurrMonth = function().getPreviousMonthBasedOnDifferent()['month'];
                        Selection.prevCurrYear = function().getPreviousMonthBasedOnDifferent()['year'];
                      }
                    }
                  });
                });
              },
            ),
            SizedBox(
              width: 50,
            ),
            DropdownButton(
            key: UniqueKey(),
            underline: Container(
              height: 2,
              color: Colors.black,
            ),
            icon: Visibility(visible: false, child: Icon(Icons.arrow_downward)),
            items: years
                .map((item) => DropdownMenuItem<String>(
                      value: item,
                      enabled: item == val? false:true,
                      child: Text(
                        item,
                        style: TextStyle(
                          fontSize: 14,
                          color: item == val ? Colors.grey : Colors.black,
                        ),
                        overflow: TextOverflow.ellipsis,
                      ),
                    ))
                .toList(),
            hint: Text("Pilih Tahun"),
            value: (Selection.currYear==-1?null:Selection.currYear.toString()),
            onChanged: (value) {
              setState(() {
                Selection.currYear = int.parse(value??"0");
                Selection.prevNextYear = Selection.currYear;

                int indexMonth = months.indexOf(Selection.currMonth);
                years.asMap().forEach((index, element) {
                  if (element == Selection.currYear.toString()) {
                    if (indexMonth > 6) {
                      int newYear = Selection.currYear + 1;
                      Selection.nextYear = newYear;
                    } else {
                      Selection.nextYear = Selection.currYear;
                    }
                  }
                });
              });
            })
          ],
        ),

        Row(
          children: [
            Text('Sampai'),
            SizedBox(
              width: 25,
            ),
            IgnorePointer(
              ignoring: true,
              child: DropdownButton(
                  // isExpanded: true,
                  hint: Text("Pilih Bulan"),
                  underline: Container(
                    height: 2,
                    color: Colors.black,
                  ),
                  icon: Visibility(visible: false, child: Icon(Icons.arrow_downward)),
                  items: months
                      .map((item) => DropdownMenuItem<String>(
                            value: item,
                            child: Text(
                              item,
                              style: const TextStyle(
                                fontSize: 14,
                                color: Colors.grey,
                              ),
                              overflow: TextOverflow.ellipsis,
                            ),
                          ))
                      .toList(),
                  value: (Selection.nextMonth.isEmpty
                      ? null
                      : Selection.nextMonth.capitalize()),
                  onChanged: (value) {
                    setState(() {
                    });
                  }),
            ),
            SizedBox(
              width: 50,
            ),
            IgnorePointer(
              ignoring: true,
              child: DropdownButton(
                  underline: Container(
                    height: 2,
                    color: Colors.black,
                  ),
                  icon: Visibility(visible: false, child: Icon(Icons.arrow_downward)),
                  items: years
                      .map((item) => DropdownMenuItem<String>(
                            value: item,
                            child: Text(
                              item,
                              style: const TextStyle(
                                fontSize: 14,
                                color: Colors.grey,
                              ),
                              overflow: TextOverflow.ellipsis,
                            ),
                          ))
                      .toList(),
                  hint: Text("Pilih Tahun"),
                  value: (Selection.nextYear == -1 ? null : Selection.nextYear.toString()),
                  onChanged: (value) {
                    setState(() {
                    });
                  }),
            )
          ],
        )
      ],
    );
  }
}

class DropDownYears extends StatefulWidget {
  const DropDownYears({super.key});

  @override
  State<DropDownYears> createState() => _DropDownYearsState();
}

class _DropDownYearsState extends State<DropDownYears> {
  String val = years[0];
  @override
  Widget build(BuildContext context) {
    if(Selection.tabController.index==3){
      Selection.nextMonth = Selection.currMonth;
      Selection.nextYear = Selection.currYear + 1;
    }
    
    return Column(
      children: [
        
        Row(
          children: [
            Text('Dari'),
            SizedBox(
              width: 50,
            ),
            DropdownButton(
          // isExpanded: true,
              hint: Text("Pilih Bulan"),
              underline: Container(
                height: 2,
                color: Colors.black,
              ),
              icon: Visibility(visible: false, child: Icon(Icons.arrow_downward)),
              items: months
                  .map((item) => DropdownMenuItem<String>(
                        value: item,
                        child: Text(
                          item,
                          style: const TextStyle(
                            fontSize: 14,
                            color: Colors.black,
                          ),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ))
                  .toList(),
              value: (Selection.currMonth.isEmpty
                  ? null
                  : Selection.currMonth.capitalize()),
              onChanged: (value) {
                int newindex = 0;
                setState(() {
                  Selection.selectedTab = 3;
                  Selection.currMonth = value as String;
                  Selection.nextMonth = value as String;
                  Selection.prevNextMonth = Selection.currMonth;
                  Selection.prevNextYear = Selection.currYear;
                  Selection.prevCurrMonth = function().getPreviousMonthBasedOnDifferent()['month'];
                  Selection.prevCurrYear = function().getPreviousMonthBasedOnDifferent()['year'];
                  
                });
              },
            ),
            SizedBox(
              width: 50,
            ),
            DropdownButton(
            key: UniqueKey(),
            underline: Container(
              height: 2,
              color: Colors.black,
            ),
            icon: Visibility(visible: false, child: Icon(Icons.arrow_downward)),
            items: years
                .map((item) => DropdownMenuItem<String>(
                      value: item,
                      enabled: item == val? false:true,
                      child: Text(
                        item,
                        style: TextStyle(
                          fontSize: 14,
                          color: item == val ? Colors.grey: Colors.black,
                        ),
                        overflow: TextOverflow.ellipsis,
                      ),
                    ))
                .toList(),
            hint: Text("Pilih Tahun"),
            value: (Selection.currYear==-1?null:Selection.currYear.toString()),
            onChanged: (value) {
              setState(() {
                int val = int.parse(value??"0");
                Selection.nextYear = val +1;
                Selection.currYear = int.parse(value??"0");
              });
            })
          ],
        ),

        Row(
          children: [
            Text('Sampai'),
            SizedBox(
              width: 25,
            ),
            IgnorePointer(
              ignoring: true,
              child: DropdownButton(
                  // isExpanded: true,
                  hint: Text("Pilih Bulan"),
                  underline: Container(
                    height: 2,
                    color: Colors.black,
                  ),
                  icon: Visibility(visible: false, child: Icon(Icons.arrow_downward)),
                  items: months
                      .map((item) => DropdownMenuItem<String>(
                            value: item,
                            child: Text(
                              item,
                              style: const TextStyle(
                                fontSize: 14,
                                color: Colors.grey,
                              ),
                              overflow: TextOverflow.ellipsis,
                            ),
                          ))
                      .toList(),
                  value: (Selection.nextMonth.isEmpty
                      ? null
                      : Selection.nextMonth.capitalize()),
                  onChanged: (value) {
                    setState(() {
                    });
                  }),
            ),
            SizedBox(
              width: 50,
            ),
            IgnorePointer(
              ignoring: true,
              child: DropdownButton(
                  underline: Container(
                    height: 2,
                    color: Colors.black,
                  ),
                  icon: Visibility(visible: false, child: Icon(Icons.arrow_downward)),
                  items: years
                      .map((item) => DropdownMenuItem<String>(
                            value: item,
                            child: Text(
                              item,
                              style: const TextStyle(
                                fontSize: 14,
                                color: Colors.grey,
                              ),
                              overflow: TextOverflow.ellipsis,
                            ),
                          ))
                      .toList(),
                  hint: Text("Pilih Tahun"),
                  value: (Selection.nextYear == -1 ? null : Selection.nextYear.toString()),
                  onChanged: (value) {
                    setState(() {
                    });
                  }),
            )
          ],
        )
      ],
    );
  }
}
