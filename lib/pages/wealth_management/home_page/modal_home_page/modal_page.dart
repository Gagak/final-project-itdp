
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'dart:math';


class ListComparison extends StatelessWidget {
  List<Map<String,dynamic>> list;
  ListComparison({
    Key? key, required this.list
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var formatter = NumberFormat.currency(locale: 'id_ID', symbol: '. ', decimalDigits: 0);
    final kategori = list;
    print('kategori $list');

    int idxOfTheMaxNow = kategori.indexWhere((element) => element['now'] == kategori.map((e) => e['now'] as int).reduce(max));
    int idx = kategori.indexWhere((element) => element['now'] == kategori.map((e) => e['then'] as int).reduce(max));
    
    return Expanded(
      child: ListView.builder(
        itemCount: 7,
        itemBuilder: (BuildContext context, int index) {
          return Column(
            children: [
              Container(
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey.shade300, width: 2),
                  color: (kategori[index]['now'] as int) > (kategori[index]['then'] as int) ? Colors.green[50] : Colors.red[50],
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(kategori[index]['name'].toString(), style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),),
                        Text("${((((kategori[index]['now'] as int)- (kategori[index]['then'] as int))/(kategori[index]['now'] as int))*100).toStringAsFixed(0)}%", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18, color: (kategori[index]['now'] as int) > (kategori[index]['then'] as int) ? Colors.green : Colors.red ),),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          decoration: BoxDecoration(
                            color: (kategori[index]['now'] as int) > (kategori[index]['then'] as int) ? Colors.greenAccent[700] : Colors.redAccent[700],
                            borderRadius: BorderRadius.circular(5),
                          ),
                          padding: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                          margin: EdgeInsets.symmetric(vertical: 10),
                          child: Text("This Month", style: TextStyle(fontSize: 12, color: Colors.white, fontWeight: FontWeight.bold),),
                        ),
                        Text("Rp${formatter.format(kategori[index]['now'])}", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: (kategori[index]['now'] as int) > (kategori[index]['then'] as int) ? Colors.green : Colors.red ),),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          decoration: BoxDecoration(
                            color: (kategori[index]['now'] as int) > (kategori[index]['then'] as int) ? Colors.greenAccent[700] : Colors.redAccent[700],
                            borderRadius: BorderRadius.circular(5),
                          ),
                          padding: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                          margin: EdgeInsets.symmetric(vertical: 10),
                          child: Text("Previous Month", style: TextStyle(color: Colors.white, fontSize: 12, fontWeight: FontWeight.bold),),
                        ),
                        Text("Rp${formatter.format(kategori[index]['then'])}", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: (kategori[index]['now'] as int) > (kategori[index]['then'] as int) ? Colors.green : Colors.red ),),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          );
        }
      ),
    );
  }
}

Container mainContainer(
  String kat,
  String prevKat,
  String period,
  double untung,
  int money,
) {
  var formatter = NumberFormat("#,###.00");

  print('untung $untung money $money, period $period, kat $kat, prevKat $prevKat');
  return Container(
    margin: EdgeInsets.only(bottom: 10),
    padding: EdgeInsets.symmetric(vertical: 20),
    decoration: BoxDecoration(
      color: Colors.orange[400],
      borderRadius: BorderRadius.circular(10),
    ),
    child: Column(
      children: [
        Text(
          "Your Spending Analyst",
          style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
        ),
        SizedBox(height: 10),
        Text('Rp${formatter.format(money)}',
          style: TextStyle(fontSize: 24, fontWeight: FontWeight.w900),
        ),
        SizedBox(height: 10),
        SizedBox(
          height: 2, 
          width: 400,
          child: Container(
            color: Colors.white,
          ),
        ),
        SizedBox(height: 10),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
              Text(
                "Compare to $period",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 10),
              Text(
                "This month you spend most on",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 10),
              Text(
                "Previous Month",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "${untung.toStringAsFixed(1)}%+",
                  style: TextStyle(fontWeight: FontWeight.bold, color: Colors.red),
                ),
                SizedBox(height: 10),
                Text("$kat"),
                SizedBox(height: 10),
                Text("$prevKat"),
              ],
            )
          ],
        ),
      ],
    ),
  );
}
