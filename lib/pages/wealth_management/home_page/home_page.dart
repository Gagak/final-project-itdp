import 'package:buttons_tabbar/buttons_tabbar.dart';
import 'package:final_project_itdp/helper/ApiHelper.dart';
import 'package:final_project_itdp/model/model_transaction_by_date.dart';
import 'package:final_project_itdp/model/transaction_by_date.dart';
import 'package:final_project_itdp/pages/wealth_management/home_page/data_dummy.dart';
import 'package:final_project_itdp/pages/wealth_management/home_page/modal_home_page/periodic_modal.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_custom_dialog/flutter_custom_dialog.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'dart:math';
import '../constants.dart';
import 'modal_home_page/modal_page.dart';
import 'dart:convert';


class HomePage extends StatefulWidget {
  HomePage({super.key});
  final dataDummy = dummy;
  final List<Map<String, Object>> month = bulan;
  final color = colorKategori;
  // late List<TransactionByDate> transactions;
  // late List<TransactionByDate> prevTransactions;

  @override
  State<HomePage> createState() => _HomePageState();
}




//callback function for selectedCurrYear, selectedCurrMonth, selectedNextYear, selectedNextMonth
class Selection extends ChangeNotifier {
  static List<List<Map<String, String>>> dataDummy = dummy;
  static int _currYear = 0;
  static String _currMonth = "";
  static int _nextYear = 0;
  static String _nextMonth = "";
  static int _selectedTab = -1;
  static late TabController _tabController;
  static String _prevCurrMonth = "";
  static String _prevNextMonth = "";
  static int _prevCurrYear = 0;
  static int _prevNextYear = 0;

  

  static getTheYear(Map<String, String> mth) {
    String yearPicked = mth['Tgl_Transaksi'].toString().split("-")[0];
    return int.parse(yearPicked);
  }

  static int get currYear => _currYear;
  static String get currMonth => _currMonth;
  static int get nextYear => _nextYear;
  static String get nextMonth => _nextMonth;
  static int get selectedTab => _selectedTab;
  static TabController get tabController => _tabController;
  static String get prevCurrMonth => _prevCurrMonth;
  static String get prevNextMonth => _prevNextMonth;
  static int get prevCurrYear => _prevCurrYear;
  static int get prevNextYear => _nextYear;


  static set currYear(int value) => _currYear = value;
  static set currMonth(String value) => _currMonth = value;
  static set nextYear(int value) => _nextYear = value;
  static set nextMonth(String value) => _nextMonth = value;
  static set selectedTab(int value) => _selectedTab = value;
  static set tabController(TabController value) => _tabController = value;
  static set prevCurrMonth(String value) => _prevCurrMonth = value;
  static set prevNextMonth(String value) => _prevNextMonth = value;
  static set prevCurrYear(int value) => _prevCurrYear = value;
  static set prevNextYear(int value) => _prevNextYear = value;
  static getAllYear() {
    List<String> year = [];
    for (int i = 0; i < dataDummy.length; i++) {
      dataDummy[i].forEach((element) {
        if (!year.contains(getTheYear(element).toString())) {
          year.add(getTheYear(element).toString());
        }
      });
    }

    //get the index of highest value of year
    int index = year.indexWhere((element) =>
        element == (year.map(int.parse).toList().reduce(max)).toString());
    year.add((int.parse(year[index]) + 1).toString());
    year.add((int.parse(year[index]) - 1).toString());
    return year;
  }

  static List<String> _listYear = getAllYear();
  //sort the list of year
  static List<String> get listYear =>
      _listYear..sort((a, b) => int.parse(b).compareTo(int.parse(a)));
}

//extension capitalize string
extension StringExtension on String {
  String capitalize1() {
    return "${this[0].toUpperCase()}${this.substring(1)}";
  }
}

class _HomePageState extends State<HomePage>
    with TickerProviderStateMixin {
  String selectedCurrMonth = '';
  int selectedCurrYear = -1;
  String selectedNextMonth = '';
  int selectedNextYear = -1;

  bool increase = false;
  late TabController _controller;
  int selectedTabIdx = -1;
  int selectedTab = -1;

  String selectedPrevCurrMonth = '';
  int selectedPrevCurrYear = -1;
  String selectedPrevNextMonth = '';
  int selectedPrevNextYear = -1;


  List<TransactionByDate> currTransactions = [];
  List<TransactionByDate> prevTransactions = [];

  late AnimationController animationController;
  
  @override
  void initState() {
    super.initState();
    selectedCurrMonth = "januari";
    selectedNextMonth = "januari";
    selectedCurrYear = 2022;
    selectedNextYear = 2023;
    selectedPrevNextMonth = "december";
    selectedPrevCurrMonth = "december";
    selectedPrevCurrYear = 2020;
    selectedPrevNextYear = 2020;

    selectedTab = 0;
    Selection._currMonth = selectedCurrMonth;
    Selection._nextMonth = selectedNextMonth;
    Selection._currYear = selectedCurrYear;
    Selection._nextYear = selectedNextYear;
    Selection._selectedTab = selectedTab;
    Selection._prevCurrMonth = selectedPrevCurrMonth;
    Selection._prevNextMonth = selectedPrevNextMonth;
    Selection._prevCurrYear = selectedPrevCurrYear;
    Selection._prevNextYear = selectedPrevNextYear;
    _controller = TabController(
        vsync: this, length: 4, initialIndex: Selection.selectedTab);
    Selection.tabController = _controller;
    animationController = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 5),
    )..addListener(() {
        setState(() {});
      });

   
    
    
  }

  
  bool isLoaded = false;

  @override
  didChangeDependencies() {
    if(!isLoaded){
      Provider.of<ModelTransactionByDate>(context).getAllCurrTransactionByDate(buildStartDate(Selection.currMonth,Selection.nextMonth,Selection.currYear,Selection.nextYear));
      Provider.of<ModelTransactionByDate>(context).getAllPrevTransactionByDate(buildStartDate(Selection.prevCurrMonth,Selection.prevNextMonth,Selection.prevCurrYear,Selection.prevNextYear));
      isLoaded = true;
      
    }
    super.didChangeDependencies();
    
  }

  getLastDateOfSelectedNextMonthAndSelectedNextYear(String mth, int year){
    int idx = widget.month.indexWhere((element) => element['next'] == mth);
    if(idx<7){
      if(idx%2==0 || idx==0){
        return 31;
      }
      else if(idx==1){
        if(year%4==0){
          return 29;
        }
        else{
          return 28;
        }
      }
      else{
        return 30;
      }
    }
    else{
      if(idx%2!=0){
        return 31;
      }
      else{
        return 30;
      }
    }
  }
  
  buildStartDate(String currMonth, String nextMonth, int currYear, int nextYear){
    String startDate = currYear.toString() + "-" + getValueOfMonth(currMonth.toString()) + "-01"; 
    String endDate = nextYear.toString() + "-" + getValueOfMonth(nextMonth.toString()) + "-" + getLastDateOfSelectedNextMonthAndSelectedNextYear(nextMonth, nextYear).toString();
    print("AA start date $startDate end date $endDate");
    return startDate + " " + endDate;
  }

  getColor(String clr){
    print("CC $clr");
    int idx = widget.color.indexWhere((element) => element['nama'] == clr);
    return widget.color[idx]['color'];
  }

  List<PieChartSectionData> showingSections(int currYear, String currMonth, int nextYear, String nextMonth) {
    int sum = 1000;
    List<Map<String, dynamic>> totalPerKategori = getTotalPerKategori(currTransactions);

    return List.generate(totalPerKategori.length, (i) {
      double val = (totalPerKategori[i]['total'] / getSumTotalPerKategori(currTransactions)) * 100;
      return PieChartSectionData(
        color: getColor(totalPerKategori[i]['kategori']),
        value: val,
        title: (''),
        radius: 60,
        titleStyle: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.bold,
            color: const Color(0xffffffff)),
      );
    });
  }

  getValueOfMonth(String month){
    int idx = widget.month.indexWhere((element) { 
      return element['name'] == month;
    });
    
    return widget.month[idx]['value'];
  }
  
  getAllIdPerKategori(List<TransactionByDate> lst, String kategori){
    List<int> id = [];
    lst.forEach((element) {
      if(element.categoryName==kategori){
        id.add(element.id);
      }
    });
    return id;
  }
  
  getTotalPerKategori(List<TransactionByDate> lst){
    List<Map<String, dynamic>> totalPerKategori = [];
    List<String> kategori = [];
    if(lst.isEmpty){
      totalPerKategori.add({'kategori': 'Tidak ada kategori', 'total': -1});
    }
    else{
      lst.forEach((element){
        if(!kategori.contains(element.categoryName)){
          kategori.add(element.categoryName);
          totalPerKategori.add(
            {
              'id': getAllIdPerKategori(lst, element.categoryName),
              'kategoriId': element.categoryId,
              'kategori': element.categoryName,
              'total': element.totalPrice
            }
          );
        }
      });
    }
    return totalPerKategori;
  }

  getSumTotalPerKategori(List<TransactionByDate> lst){
    int sum = 0;
    List<Map<String, dynamic>> totalPerKategori = getTotalPerKategori(lst);
    totalPerKategori.forEach((element) {
      if(element['total']!=-1){
        int total = element['total'];
        int idLength = element['id'].length;
        sum += total*idLength;
      }
      
    });
    return sum;
  }

  getPercentageOfDifferenceOfTotalPriceForTransaction(){
    int currSum = getSumTotalPerKategori(currTransactions);
    int prevSum = getSumTotalPerKategori(prevTransactions);
    increase = currSum<prevSum;
    if(prevSum==0){
      return 100;
    }
    else{
      return ((currSum - prevSum)/prevSum)*100;
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  showALertDialog() {
    YYDialog().build(context)
      ..width = 370
      ..height = 281
      ..decoration = BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.white,
          border: Border.all(width: 3.0, color: Colors.grey))
      ..widget(
        Padding(
          padding: EdgeInsets.symmetric(vertical: 30.0),
          child: Align(
            alignment: Alignment.centerLeft,
            child: Center(
              child: Column(
                children: [
                  Text(
                    "Pilih Periode Yang Diinginkan",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 14.0,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        DefaultTabController(
                            initialIndex: 0,
                            length: 4,
                            child: Column(
                              children: [
                                ButtonsTabBar(
                                  buttonMargin:
                                      EdgeInsets.symmetric(horizontal: 5),
                                  height: 30,
                                  controller: _controller,
                                  borderWidth: 1.4,
                                  borderColor: Colors.grey,
                                  unselectedLabelStyle: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w500),
                                  labelStyle: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w500),
                                  unselectedBackgroundColor: Colors.white,
                                  unselectedBorderColor: Colors.grey,
                                  backgroundColor:
                                      Color.fromARGB(255, 250, 155, 35),
                                  tabs: [
                                    Tab(
                                      text: "  1 Bulan  ",
                                    ),
                                    Tab(
                                      text: "  3 Bulan  ",
                                    ),
                                    Tab(
                                      text: "  6 Bulan  ",
                                    ),
                                    Tab(
                                      text: "  1 Tahun  ",
                                    ),
                                  ],
                                )
                              ],
                            )),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10, top: 15),
                    child: SizedBox(
                      height: 96,
                      width: 500,
                      child: TabBarView(controller: _controller, children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Text("Dari"),
                            DropDown1(),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 20.0),
                          child: DropdDown3(),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 20.0),
                          child: DropDown6(),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 20),
                          child: DropDownYears(),
                        ),
                      ]),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      )
      ..doubleButton(
        padding: EdgeInsets.symmetric(vertical: 30),
        gravity: Gravity.right,
        text2: "OK",
        color2: Color.fromARGB(255, 250, 155, 35),
        fontSize1: 14.0,
        fontWeight1: FontWeight.w500,
        onTap2: () {
          setState(() {
            selectedCurrMonth = Selection._currMonth;
            selectedCurrYear = Selection._currYear;
            selectedNextMonth = Selection._nextMonth;
            selectedNextYear = Selection._nextYear;
            selectedTab = Selection._selectedTab;
          });
        },
      )
      ..show();
  }
 int idx = 0;
 
  @override
  Widget build(BuildContext context) {
    
    
    currTransactions = Provider.of<ModelTransactionByDate>(context).currTransactionByDate;
    prevTransactions = Provider.of<ModelTransactionByDate>(context).prevTransactionByDate;
      
    List<Map<String, dynamic>> totalPerKategori = getTotalPerKategori(currTransactions);
    List<Map<String, dynamic>> totalPerKategoriPrev = getTotalPerKategori(prevTransactions);
    print("A $idx TOTAL-PER-KATEGORI ${totalPerKategori}");
    print("A $idx TOTAL-PER-KATEGORI-PREV ${totalPerKategoriPrev}");
    print("A $idx SUM TOTAL ${getSumTotalPerKategori(currTransactions)}");
    print("A $idx GET PERCENTAGE ${getPercentageOfDifferenceOfTotalPriceForTransaction()}");
    idx++;
      return Scaffold(
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.orange,
          onPressed: () {},
          child: Icon(
            Icons.add_circle_outline,
            color: Colors.white,
            size: 50,
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        body: Container(
          child: Column(
            children: [
              ListTile(
                contentPadding:
                    const EdgeInsets.symmetric(vertical: 6, horizontal: 20),
                leading: Container(
                  height: 25,
                  width: 25,
                  child: TextButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    style: backButtonStyle,
                    child: const Text("<"),
                  ),
                ),
                title: Column(
                  children: const [
                    Text(
                      'Smart Financial',
                      style: TextStyle(
                          fontFamily: "Arial", fontWeight: FontWeight.bold),
                    ),
                    Text(
                      'Hi Haikal Ravendy Yusmananda',
                      style: TextStyle(
                          fontFamily: "Arial", fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 15),
                child: Stack(
                  children: [
                    Container(
                      height: 260,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: Offset(0, 3), // changes position of shadow
                          ),
                        ],
                      ),
                      child: Column(
                        children: [
                          const SizedBox(
                            height: 160,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              MyButton(
                                color: Color.fromRGBO(79, 100, 156, 1),
                                imgFileName: 'Wishlist',
                                subtitle: 'Wishlist',
                                target: 'wishlist',
                              ),
                              MyButton(
                                color: Color.fromRGBO(77, 186, 69, 1),
                                imgFileName: 'Scan',
                                subtitle: 'Scan',
                                target: 'bill_history',
                              ),
                              Column(
                                children: [
                                  SizedBox(
                                    height: 10,
                                  ),
                                  MyButton(
                                    color: Color.fromRGBO(51, 165, 251, 1),
                                    imgFileName: 'Encyclopedia',
                                    subtitle: 'Literature \n Keuangan',
                                    target: 'portal_ojk',
                                  ),
                                ],
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                    ComparisonPeriodeWidget(now: getTotalPerKategori(currTransactions),then:getTotalPerKategori(prevTransactions),total: getSumTotalPerKategori(currTransactions), percentage: getPercentageOfDifferenceOfTotalPriceForTransaction(), text: increase?"Ayo lebih hemat lagi!": "Selamat! Ayo jangan lengah dan terus menabung", increase: increase!,)
                  ],
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Container(
                height: 20,
                color: Colors.grey[300],
              ),

              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: Colors.white,
                      onPrimary: Colors.black,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5),
                          side: const BorderSide(
                            color: Colors.black,
                            width: 1,
                          )),
                    ),
                    onPressed: () {
                      return showALertDialog();
                    },
                    child: Text(selectedCurrMonth == selectedNextMonth &&
                            selectedCurrYear == selectedNextYear
                        ? '${selectedCurrMonth.capitalize1()} ${selectedCurrYear}'
                        : '${selectedCurrMonth.capitalize1()} ${selectedCurrYear} - ${selectedNextMonth.capitalize1()} ${selectedNextYear}')),
              ),

              Container(
                  height: 170,
                  child: Expanded(
                    child: PieChart(
                      PieChartData(
                          startDegreeOffset: 180,
                          centerSpaceRadius: 0,
                          sections: showingSections(selectedCurrYear, selectedCurrMonth, selectedNextYear, selectedNextMonth),
                      ),
                    ),
                  ),
                ),

              Expanded(
                child: ListView.builder(
                    itemCount: totalPerKategori.length,
                    itemBuilder: (context, index) {
                      return Container(
                        child: Column(
                          children: [
                            Container(
                              child: Column(
                                children: [
                                  ItemSatuan(
                                    res: buildStartDate(selectedCurrMonth, selectedNextMonth, selectedCurrYear, selectedNextYear),
                                    warna:
                                        getColor(totalPerKategori[index]['kategori']),
                                    kategori: totalPerKategori[index]['kategori']
                                        .toString(),
                                    nominal:
                                        totalPerKategori[index]['total'],
                                    percentase: (totalPerKategori[index]
                                            ['total'] /
                                        getSumTotalPerKategori(currTransactions) *
                                        100) as double,
                                    categoryId: totalPerKategori[index]['kategoriId'],
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      );
                    }),
              )
            ],
          ),
        ),
      );  
  }
}

class MyButton extends StatelessWidget {
  Color color;
  String imgFileName;
  String subtitle;
  String target;
  MyButton(
      {super.key,
      required this.color,
      required this.imgFileName,
      required this.subtitle,
      required this.target});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ElevatedButton(
          onPressed: () {
            Navigator.pushNamed(context, "/$target");
          },
          style: ElevatedButton.styleFrom(
            fixedSize: Size(50, 50),
            backgroundColor: color,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
          ),
          child: Center(
            child: Image.asset(
              'assets/images/$imgFileName.png',
              color: Colors.white,
              width: 30,
              height: 30,
            ),
          ),
        ),
        SizedBox(
          height: 5,
        ),
        Text(
          subtitle,
          style: TextStyle(color: Colors.grey[700]),
        )
      ],
    );
  }
}

class ComparisonPeriodeWidget extends StatelessWidget {
  List<Map<String, dynamic>> now;
  List<Map<String, dynamic>> then;
  int total;
  double percentage;
  String text;
  bool increase;
  ComparisonPeriodeWidget(
      {super.key,
      required this.total,
      required this.percentage,
      required this.text,
      required this.increase,
      required this.now,
      required this.then});

  getCat(List<Map<String, dynamic>> now, List<Map<String, dynamic>> then) {
    List<Map<String, dynamic>> cat = [];
    print("AA then ${then.length}");
    int length = now.length >= then.length ? now.length : then.length;
    for (int i = 0; i < length; i++) {
      cat.add({
        'name': now.length==0? "Tidak ada Kategori" : now[i]['kategori'].toString(),
        'now': now.length==0? 0 : now[i]['total']!,
        'then': then.length==0? 0 : then[i]['total']!,
      });
    }
    print("AA get cat $cat");
    return cat;
  }

  getIDofMax(List<Map<String, dynamic>> obj, String text) {
    int max = 0;
    int id = 0;
    for (int i = 0; i < obj.length; i++) {
      int tot = obj[i]['total'];
      if (tot > max) {
        max = obj[i]['total'];
        id = i;
      }
    }
    return id;
  }

  @override
  Widget build(BuildContext context) {
    print("AA now $now");
    List<Map<String, dynamic>> cat = getCat(now, then);

    var formatter =
        NumberFormat.currency(locale: 'id_ID', symbol: '. ', decimalDigits: 0);

    //get the idx of the highest now value

    int idxOfTheMaxNow = getIDofMax(now, 'now');
    print("AA idx of the max now $idxOfTheMaxNow");
    int idx = getIDofMax(then, 'then');
    print("AA idx of the max then $idx");
    return TextButton(
      style: TextButton.styleFrom(
          backgroundColor: Colors.orange.withOpacity(0.5),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          shadowColor: Colors.grey,
          fixedSize: Size(double.infinity, 150)),
      onPressed: () {
        showDialog<String>(
          context: context,
          builder: (BuildContext context) => Dialog(
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10)),
            ),
            child: Column(children: [
              mainContainer(cat[idxOfTheMaxNow]['name'].toString(),
                  cat[idx]['name'].toString(), "last month", percentage, total),
              ListComparison(
                list: cat,
              ),
            ]),
          ),
        );
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Column(
                children: [
                  Text(
                    "Total",
                    style: TextStyle(color: Colors.black, fontFamily: "Arial"),
                  ),
                  Text(
                    "Rp${formatter.format(total).toString()}",
                    style: TextStyle(
                        color: Colors.black,
                        fontFamily: "Arial",
                        fontSize: 20,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              ),
              Column(
                children: [
                  Text(
                    "Compare to last period",
                    style: TextStyle(color: Colors.black, fontFamily: "Arial"),
                  ),
                  Text(
                    increase
                        ? '- ${(percentage*-1).toStringAsFixed(2)}%'
                        : '+ ${percentage.toStringAsFixed(2)}%',
                    style: TextStyle(
                        color: increase ? Colors.red : Colors.green,
                        fontFamily: "Arial",
                        fontSize: 20,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              )
            ],
          ),
          Text(
            text,
            style: TextStyle(
                color: Colors.black, fontSize: 15, fontWeight: FontWeight.bold),
          )
        ],
      ),
    );
  }
}

class ItemSatuan extends StatelessWidget {
  final Color warna;
  final String kategori;
  final int nominal;
  final double? percentase;
  final String res;
  final String categoryId;
  const ItemSatuan(
      {required this.warna,
      required this.kategori,
      required this.nominal,
      required this.percentase,
      required this.res,
      required this.categoryId});
  // : super(key: key);

  @override
  Widget build(BuildContext context) {
    var formatter =
        NumberFormat.currency(locale: 'id_ID', symbol: '', decimalDigits: 0);
    return ListTile(
        onTap: () {
          Navigator.pushNamed(context, "/transaksi_history");
        },
        leading: CircleAvatar(
          radius: 20,
          child: FittedBox(
            child: Text('${percentase!.toStringAsFixed(1)}%', style: TextStyle(fontSize:13 ),),
          ),
          backgroundColor: warna,
        ),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text('$kategori'),
            Text('- Rp. ${formatter.format(nominal)}')
          ],
        ));
  }
}
