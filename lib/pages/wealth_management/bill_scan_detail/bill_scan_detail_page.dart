import 'package:final_project_itdp/model/DetailBillTransaction.dart';
import 'package:final_project_itdp/model/ListBillTransaction.dart';
import 'package:final_project_itdp/model/ModelDetailBillTransaction.dart';
import 'package:final_project_itdp/model/ModelListBillTransaction.dart';
import 'package:final_project_itdp/pages/wealth_management/bill_scan_detail/widgets/CardScanDetail.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../constants.dart';

class BillScanDetail extends StatefulWidget {
  late DetailBillTransaction detailBillTransaction;

  BillScanDetail({super.key});

  @override
  State<BillScanDetail> createState() => _BillScanDetailState();
}

class _BillScanDetailState extends State<BillScanDetail> {
  @override
  Widget build(BuildContext context) {
    widget.detailBillTransaction =
        Provider.of<ModelDetailBillTransaction>(context).selected;

    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 100.0,
        leading: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: const EdgeInsets.all(13.0),
              height: 25,
              width: 25,
              child: TextButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                style: backButtonStyle,
                child: const Text("<"),
              ),
            ),
          ],
        ),
        backgroundColor: warnaAppBar,
        elevation: 0.0,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(15),
          ),
        ),
        title: const Text(
          "Scan Bill",
          style: TextStyle(
              fontSize: 18,
              color: warnaTextPrimary,
              fontWeight: FontWeight.w700),
        ),
        centerTitle: true,
      ),
      body: Column(
        children: [
          const Padding(
            padding: EdgeInsets.symmetric(vertical: 20.0),
            child: Text(
              "Detail Transaksi",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w900),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 10.0),
            height: 30,
            decoration: const BoxDecoration(
              color: warnaTextLowOpacity,
            ),
          ),
          CardScanDetail(
            detailBillTransaction: widget.detailBillTransaction,
          ),
        ],
      ),
    );
  }
}
