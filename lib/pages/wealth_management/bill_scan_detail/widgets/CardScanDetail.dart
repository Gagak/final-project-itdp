import 'package:final_project_itdp/model/DetailBillTransaction.dart';
import 'package:final_project_itdp/model/ListBillTransaction.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

import '../../constants.dart';

class CardScanDetail extends StatelessWidget {
  DetailBillTransaction detailBillTransaction;

  CardScanDetail({required this.detailBillTransaction});

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 40),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(style: BorderStyle.solid, width: 2),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(
                    horizontal: 25.0, vertical: 10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          detailBillTransaction.merchantName,
                          style: const TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 20),
                          width: 40,
                          height: 40,
                          child: Image.asset('assets/images/bill.png'),
                        ),
                      ],
                    ),
                    Text(
                      " ${detailBillTransaction.transactionDate}",
                      style: const TextStyle(fontWeight: FontWeight.w500),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Text(
                      "Rp${detailBillTransaction.totalPrice.toString()}",
                      style: const TextStyle(
                          color: warnaTextOrange,
                          fontSize: 30.0,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
              Flexible(
                child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: detailBillTransaction.detailsByCategory.length,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 20,
                      ),
                      child: Container(
                        margin: const EdgeInsets.all(10.0),
                        padding: EdgeInsets.all(20.00),
                        decoration: BoxDecoration(
                          border: Border.all(
                            style: BorderStyle.solid,
                          ),
                          borderRadius: BorderRadius.circular(11),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(1),
                              spreadRadius: 1,
                              blurRadius: 5,
                              offset: Offset(0, 2),
                            ),
                          ],
                          color: Colors.white,
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            // Row title
                            Text(
                              detailBillTransaction.categoryByBill[index]
                                  .toString(),
                              style: const TextStyle(
                                decoration: TextDecoration.underline,
                                fontSize: 14.0,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            //@todo perlu-dibikin-listview
                            // Row(
                            //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            //   children: const [
                            //     Text(
                            //       "Pisang Sunpride",
                            //       style: TextStyle(
                            //           fontSize: 14.0,
                            //           fontWeight: FontWeight.bold),
                            //     ),
                            //     Text(
                            //       "x1, Rp.69.000",
                            //       style: TextStyle(
                            //           fontWeight: FontWeight.w900,
                            //           fontSize: 14.0),
                            //     ),
                            //   ],
                            // ),
                            // Row(
                            //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            //   children: const [
                            //     Text(
                            //       "ABC ",
                            //       style: TextStyle(
                            //           fontSize: 14.0,
                            //           fontWeight: FontWeight.bold),
                            //     ),
                            //     Text(
                            //       "x1, Rp.55.000",
                            //       style: TextStyle(
                            //           fontWeight: FontWeight.w900,
                            //           fontSize: 14.0),
                            //     ),
                            //   ],
                            // ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
