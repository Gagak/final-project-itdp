import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

import '../../constants.dart';

class TextButtons extends StatelessWidget {
  const TextButtons({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        OutlinedButton(
            style: OutlinedButton.styleFrom(
                backgroundColor: warnaAppBar,
                foregroundColor: warnaTextPrimary,
                side: BorderSide(width: 2.0, color: warnaTextLowOpacity)),
            onPressed: () {},
            child: Text("All")),
        OutlinedButton(
            style: OutlinedButton.styleFrom(
                foregroundColor: warnaTextPrimary,
                side: BorderSide(width: 2.0, color: warnaTextLowOpacity)),
            onPressed: () {},
            child: Text("Film")),
        OutlinedButton(
            style: OutlinedButton.styleFrom(
                foregroundColor: warnaTextPrimary,
                side: BorderSide(width: 2.0, color: warnaTextLowOpacity)),
            onPressed: () {},
            child: Text("Game")),
        OutlinedButton(
            style: OutlinedButton.styleFrom(
                foregroundColor: warnaTextPrimary,
                side: BorderSide(width: 2.0, color: warnaTextLowOpacity)),
            onPressed: () {},
            child: Text("Lainnya"))
      ],
    );
  }
}
