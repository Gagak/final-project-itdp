import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

class ListExpenseHistory extends StatefulWidget {
  const ListExpenseHistory({super.key});

  @override
  State<ListExpenseHistory> createState() => _ListExpenseHistoryState();
}

class _ListExpenseHistoryState extends State<ListExpenseHistory> {
  @override
  Widget build(BuildContext context) {
    final List<String> expenseDate = <String>[
      '3 November',
      '14 November',
      '25 November'
    ];
    final List<String> description = <String>[
      "Baskin Robbins - Makanan",
      "Coffee Bean - Makanan",
      "Wendy's - Makanan"
    ];
    final List<String> receiptNumber = <String>[
      "989718273817",
      "436271923831",
      "129732013901"
    ];
    final List<String> price = <String>["300.000", "100.000", "100.000"];

    return Expanded(
      child: Container(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 10.0),
          child: Column(
            children: [
              Flexible(
                child: ListView.builder(
                  itemCount: description.length,
                  itemBuilder: (BuildContext context, int index) {
                    return GestureDetector(
                      onTap: () {
                        Navigator.pushNamed(context, "/rincian");
                      },
                      child: Container(
                        margin: const EdgeInsets.symmetric(vertical: 10.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(description[index]),
                                Text("No. ${receiptNumber[index]}"),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Text(
                                  "- Rp${price[index]}",
                                  style: const TextStyle(
                                    color: Color(0xFFd9d9d9),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
