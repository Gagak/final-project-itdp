import 'package:flutter/material.dart';

class GambarHargaWidget extends StatelessWidget {
  const GambarHargaWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          margin: const EdgeInsets.only(bottom: 10.0),
          width: 100,
          height: 100,
          child: Image.asset("assets/images/makan.png"),
          decoration: BoxDecoration(
            color: Colors.amber,
            borderRadius: BorderRadius.circular(15.0),
          ),
        ),
        const Text(
          '-Rp.500.000',
          style: TextStyle(fontSize: 18),
        )
      ],
    );
  }
}
