import 'package:final_project_itdp/model/model_transaction_by_date.dart';
import 'package:final_project_itdp/model/transaction_by_date.dart';
import 'package:final_project_itdp/pages/wealth_management/expense_history_by_category/widgets/gambarHargaWidget.dart';
import 'package:final_project_itdp/pages/wealth_management/expense_history_by_category/widgets/listExpenseHistory.dart';
import 'package:final_project_itdp/pages/wealth_management/expense_history_by_category/widgets/textButton.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../constants.dart';
class valueArguments{
  final String categoryId;
  final String res;

  valueArguments(this.categoryId, this.res);
}
class ExpenseHistoryPage extends StatefulWidget {
  
  ExpenseHistoryPage({super.key});

  @override
  State<ExpenseHistoryPage> createState() => _ExpenseHistoryPageState();
}

class _ExpenseHistoryPageState extends State<ExpenseHistoryPage> {
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 100.0,
        leading: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: EdgeInsets.all(13.0),
              height: 25,
              width: 25,
              child: TextButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                style: backButtonStyle,
                child: const Text("<"),
              ),
            ),
          ],
        ),
        backgroundColor: warnaAppBar,
        elevation: 0.0,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(15),
          ),
        ),
        title: const Text(
          "Detail Pengeluaran",
          style: TextStyle(
              fontSize: 18,
              color: warnaTextPrimary,
              fontWeight: FontWeight.w700),
        ),
        centerTitle: true,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Padding(
            padding: EdgeInsets.only(top: 30.0),
            child: GambarHargaWidget(),
          ),
          const Padding(
            padding: EdgeInsets.all(20.0),
            child: TextButtons(),
          ),
          Container(
            margin: const EdgeInsets.only(top: 10.0),
            height: 30,
            decoration: const BoxDecoration(
              color: warnaTextLowOpacity,
            ),
          ),
          // Perlu pub add groupedlist
          ListExpenseHistory(),
        ],
      ),
    );
  }
}
