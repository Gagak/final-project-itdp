import 'package:final_project_itdp/model/ListBillTransaction.dart';
import 'package:final_project_itdp/model/ModelDetailBillTransaction.dart';
import 'package:final_project_itdp/model/ModelListBillTransaction.dart';
import 'package:final_project_itdp/pages/wealth_management/bill_scan_history/widgets/billCardWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:provider/provider.dart';

import '../constants.dart';

class BillScanHistoryPage extends StatefulWidget {
  const BillScanHistoryPage({super.key});

  @override
  State<BillScanHistoryPage> createState() => _BillScanHistoryPageState();
}

class _BillScanHistoryPageState extends State<BillScanHistoryPage> {
  List<ListBillTransaction> listBillTransactions = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    // Isi list
    Provider.of<ModelListBillTransaction>(context, listen: false)
        .getAllListBillTransaction();
    print("LOG: listBillTransactions = $listBillTransactions");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 100.0,
        leading: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: const EdgeInsets.all(13.0),
              height: 25,
              width: 25,
              child: TextButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                style: backButtonStyle,
                child: const Text("<"),
              ),
            ),
          ],
        ),
        backgroundColor: warnaAppBar,
        elevation: 0.0,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(15),
          ),
        ),
        title: const Text(
          "Scan Bill",
          style: TextStyle(
              fontSize: 18,
              color: warnaTextPrimary,
              fontWeight: FontWeight.w700),
        ),
        centerTitle: true,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, "/scan_bill");
        },
        child: Icon(Icons.camera_alt_sharp),
        backgroundColor: Color(0xFF3b3b3b),
      ),
      body: Column(
        children: [
          const Padding(
            padding: EdgeInsets.only(top: 30, bottom: 30),
            child: Text(
              "Scan Transaksi Terakhir",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w900),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 10.0),
            height: 30,
            decoration: const BoxDecoration(
              color: warnaTextLowOpacity,
            ),
          ),
          Expanded(
            child: Container(
              child: Column(
                children: [
                  Flexible(
                    //@todo list-view-bill-card
                    child: getListBillTransactionData(),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  getListBillTransactionData() {
    var data =
        Provider.of<ModelListBillTransaction>(context).allListBillTransactions;
    var isLoading =
        Provider.of<ModelListBillTransaction>(context).isLoadingData;
    if (isLoading) {
      return const Center(
        child: CircularProgressIndicator(),
      );
    } else if (data.length > 0) {
      return ListView.builder(
        itemCount: data.length,
        itemBuilder: (context, index) {
          // @todo masukkin-parameter
          return BillCard(
            listBillTransaction: data[index],
            onTap: () {
              print("LOG: details id = ${data[index].id}");
              Provider.of<ModelDetailBillTransaction>(context, listen: false)
                  .selectOneBillTransaction = data[index].id;
              Navigator.pushNamed(context, '/bill_history_details');
            },
          );
        },
      );
    } else {
      return const Center(
        child: Text("No data"),
      );
    }
  }
}
