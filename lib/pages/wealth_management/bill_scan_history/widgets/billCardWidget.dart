import 'dart:ui';

import 'package:final_project_itdp/model/ListBillTransaction.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';

import '../../constants.dart';

class BillCard extends StatelessWidget {
  ListBillTransaction listBillTransaction;
  var onTap;
  BillCard({required this.listBillTransaction, required this.onTap});

  @override
  Widget build(BuildContext context) {
    return Stack(children: <Widget>[
      Container(
        margin: const EdgeInsets.all(10.0),
        padding:
            const EdgeInsets.only(left: 80, right: 30, top: 10, bottom: 10),
        decoration: BoxDecoration(
          border: Border.all(
            style: BorderStyle.solid,
          ),
          borderRadius: BorderRadius.circular(11),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(1),
              spreadRadius: 1,
              blurRadius: 5,
              offset: Offset(0, 2),
            ),
          ],
          color: Colors.white,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // Row title
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  listBillTransaction.merchantName,
                  style: const TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  "${DateFormat.jm().format(listBillTransaction.createdAt!).toString()} ",
                  style: const TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),

            // Row tanggal
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 5.0),
              child: Row(
                children: [
                  Text(
                    "${DateFormat('d MMM y').format(listBillTransaction.transactionDate).toString()}",
                    style: const TextStyle(
                      color: warnaTextModerateOpacity,
                    ),
                  ),
                ],
              ),
            ),

            // Row harga
            Row(
              children: [
                Text(
                  listBillTransaction.totalPrice.toString(),
                  style: const TextStyle(
                      color: warnaTextOrange,
                      fontSize: 18.0,
                      fontWeight: FontWeight.bold),
                ),
              ],
            ),

            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // Row divider
                const Divider(
                  height: 10,
                  thickness: 2,
                  color: Colors.black,
                ),

                // Row detail & button
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 5.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        '${listBillTransaction.totalItem} item - ${listBillTransaction.totalCategory} Kategori',
                        style: const TextStyle(
                          color: warnaTextModerateOpacity,
                        ),
                      ),
                      TextButton(
                        style: ButtonStyle(
                          foregroundColor:
                              MaterialStateProperty.all<Color>(Colors.white),
                          backgroundColor: MaterialStateProperty.all<Color>(
                              Color(0xFFFA9B23)),
                          shape: MaterialStateProperty.all(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(18.0),
                              side: BorderSide(color: Colors.red),
                            ),
                          ),
                        ),
                        onPressed: onTap,
                        child: const Text('Lihat Detail'),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
      Positioned(
        top: 28,
        left: 35,
        child: Container(
          width: 40,
          height: 40,
          child: Image.asset('assets/images/bill.png'),
        ),
      ),
    ]);
  }
}
