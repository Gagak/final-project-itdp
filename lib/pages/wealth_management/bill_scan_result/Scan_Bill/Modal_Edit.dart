import 'dart:html';

import 'package:final_project_itdp/pages/wealth_management/bill_scan_result/DataDummy.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

const List<String> listMainCategory = <String>['Excluded', 'Belanja', 'Games'];
const Map<String, List<String>> listSubCategory = {
  'Excluded': ['Tidak ada'],
  'Belanja': ['Fashion', 'Ikan Cupang'],
  'Games': ['Mobile Legend', 'Free Fire', 'Genshin']
};

class MyModal {
  // TODO:
  static Future<void> CategoryEditModal(
      {Trx? editTrx, required BuildContext context}) {
    String? myValidator(String? value) {
      if (value == "0" || value == null) {
        return "Please fill this field";
      }
      return null;
    }

    String category = "";
    String subCategory = "";
    void choosenCategory(String cat) {
      category = cat;
    }

    void choosenSubCategory(String subCat) {
      subCategory = subCat;
    }

    Trx trx = Trx("Null", "Null", "", 0, 0, 0);
    final formKey = GlobalKey<FormState>();
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return Consumer<DataDummy>(
          builder: (context, billData, child) {
            return AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              titlePadding: const EdgeInsets.symmetric(vertical: 15),
              title: const Center(child: Text('Tambah Item')),
              contentPadding:
                  const EdgeInsets.symmetric(vertical: 20, horizontal: 5),
              actionsPadding: const EdgeInsets.all(0),
              content: SizedBox(
                height: 300,
                width: MediaQuery.of(context).size.width,
                child: Form(
                  key: formKey,
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: const [
                          Expanded(child: Text('Kategori')),
                          Expanded(child: Text('Sub Kategori'))
                        ],
                      ),
                      const SizedBox(
                        height: 3,
                      ),
                      DropDownCategory(
                        callbackCat: (val) => choosenCategory(val),
                        callbackSubCat: (val) => choosenSubCategory(val),
                      ),
                      const SizedBox(
                        height: 3,
                      ),
                      Expanded(
                        child: TextFormField(
                          initialValue: editTrx?.itemName,
                          decoration: const InputDecoration(
                              filled: true,
                              fillColor: Color(0xffEFEFEF),
                              hintText: 'Nama Item',
                              border: OutlineInputBorder()),
                          autofocus: true,
                          onSaved: (val) {
                            trx.itemName = val ?? "";
                          },
                        ),
                      ),
                      Expanded(
                        child: TextFormField(
                          initialValue: editTrx?.price.toString(),
                          keyboardType: TextInputType.number,
                          inputFormatters: [
                            FilteringTextInputFormatter.allow(RegExp(r'[0-9]'))
                          ],
                          validator: (value) {
                            return myValidator(value);
                          },
                          decoration: const InputDecoration(
                              filled: true,
                              fillColor: Color(0xffEFEFEF),
                              hintText: 'Harga Item',
                              border: OutlineInputBorder()),
                          autofocus: true,
                          onSaved: (val) {
                            trx.price = int.parse(val ?? "0");
                          },
                        ),
                      ),
                      Expanded(
                        child: TextFormField(
                          keyboardType: TextInputType.number,
                          initialValue: editTrx?.amount.toString(),
                          inputFormatters: [
                            FilteringTextInputFormatter.allow(RegExp(r'[0-9]'))
                          ],
                          validator: (value) {
                            return myValidator(value);
                          },
                          decoration: const InputDecoration(
                              filled: true,
                              fillColor: Color(0xffEFEFEF),
                              hintText: 'Jumlah Item',
                              border: OutlineInputBorder()),
                          autofocus: true,
                          onSaved: (val) {
                            trx.amount = int.parse(val ?? "0");
                          },
                        ),
                      ),
                      Expanded(
                        child: TextFormField(
                          keyboardType: TextInputType.number,
                          initialValue: editTrx?.diskon.toString(),
                          inputFormatters: [
                            FilteringTextInputFormatter.allow(RegExp(r'[0-9]'))
                          ],
                          decoration: const InputDecoration(
                              filled: true,
                              fillColor: Color(0xffEFEFEF),
                              hintText: 'Diskon Item',
                              border: OutlineInputBorder()),
                          autofocus: true,
                          onSaved: (val) {
                            trx.diskon = int.parse(val ?? "0");
                          },
                        ),
                      )
                    ],
                  ),
                ),
              ),
              //NOTE: BUTTON ACTION
              actions: <Widget>[
                Row(
                  // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Expanded(
                        child: SizedBox(
                      height: 40,
                      child: TextButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        style: const ButtonStyle(
                            backgroundColor:
                                MaterialStatePropertyAll(Color(0xFFD8D0D0)),
                            shape: MaterialStatePropertyAll(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        bottomLeft: Radius.circular(10))))),
                        child: const Text(
                          'Batal',
                          style: TextStyle(color: Colors.black),
                        ),
                      ),
                    )),
                    Expanded(
                        child: SizedBox(
                      height: 40,
                      child: TextButton(
                        onPressed: () {
                          final isValid = formKey.currentState!.validate();
                          if (isValid) {
                            print(trx.amount);
                            print(trx.itemName);
                            trx.cat = category;
                            trx.subCat = subCategory;
                            formKey.currentState!.save();
                            if (editTrx != null) {
                              Provider.of<DataDummy>(context, listen: false)
                                  .editBillTrx(editTrx.id, trx);
                              Navigator.pop(context);
                            } else {
                              Provider.of<DataDummy>(context, listen: false)
                                  .addBillTrx(trx);
                              Navigator.pop(context);
                            }
                          } else {
                            print("aw");
                          }
                        },
                        style: const ButtonStyle(
                            backgroundColor:
                                MaterialStatePropertyAll(Color(0xFFFDC60C)),
                            shape: MaterialStatePropertyAll(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        bottomRight: Radius.circular(10))))),
                        child: const Text(
                          'Simpan',
                          style: TextStyle(color: Colors.black),
                        ),
                      ),
                    ))
                  ],
                )
              ],
            );
          },
        );
      },
    );
  }

  // TODO: Alert Confirmation
  static Future<void> AlertBox(BuildContext context) {
    return showDialog<void>(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            actionsPadding: const EdgeInsets.all(0),
            contentPadding:
                const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
            content: ContentAlert(),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadiusDirectional.circular(10)),
            actions: <Widget>[
              Row(
                // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                      child: SizedBox(
                    height: 40,
                    child: TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      style: const ButtonStyle(
                          backgroundColor:
                              MaterialStatePropertyAll(Color(0xFFD8D0D0)),
                          shape: MaterialStatePropertyAll(
                              RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      bottomLeft: Radius.circular(10))))),
                      child: const Text(
                        'Batal',
                        style: TextStyle(color: Colors.black),
                      ),
                    ),
                  )),
                  Expanded(
                      child: SizedBox(
                    height: 40,
                    child: TextButton(
                      onPressed: () {
                        Navigator.pushNamed(context, "/bill_history");
                      },
                      style: const ButtonStyle(
                          backgroundColor:
                              MaterialStatePropertyAll(Color(0xFFFDC60C)),
                          shape: MaterialStatePropertyAll(
                              RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      bottomRight: Radius.circular(10))))),
                      child: const Text(
                        'Simpan',
                        style: TextStyle(color: Colors.black),
                      ),
                    ),
                  ))
                ],
              )
            ],
          );
        });
  }
}

// TODO: Drop Down Category
class DropDownCategory extends StatefulWidget {
  final FunctionStringCallback callbackCat;
  final FunctionStringCallback callbackSubCat;
  const DropDownCategory(
      {required this.callbackCat, required this.callbackSubCat, super.key});

  @override
  State<DropDownCategory> createState() => _DropDownCategoryState();
}

class _DropDownCategoryState extends State<DropDownCategory> {
  String? mainDropDownValue;
  String? subDropDownValue;
  List<String> mySubCategory = [];
  String firstSubCategory = 'Excluded';

  toDropDownItem(List<String> list) {
    return list.map<DropdownMenuItem<String>>((String value) {
      return DropdownMenuItem<String>(
        value: value,
        child: Text(value),
      );
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    widget.callbackCat(listMainCategory.first);
    return Row(
      children: [
        Expanded(
          child: Container(
            color: const Color(0xFFDDDDDD),
            padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
            child: DropdownButton<String>(
              value: mainDropDownValue ?? listMainCategory.first,
              icon: const Icon(Icons.keyboard_arrow_down_sharp),
              elevation: 16,
              underline: const SizedBox(),
              onChanged: (String? value) {
                // This is called when the user selects an item.
                widget.callbackCat(value ?? "null");
                setState(() {
                  mainDropDownValue = value!;
                  mySubCategory = listSubCategory[mainDropDownValue]!;
                  subDropDownValue = listSubCategory[mainDropDownValue]!.first;
                });
              },
              isExpanded: true,
              items: listMainCategory
                  .map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
            ),
          ),
        ),
        Expanded(
          child: Container(
            color: const Color(0xFFDDDDDD),
            padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
            child: DropdownButton<String>(
              value:
                  subDropDownValue ?? listSubCategory[firstSubCategory]!.first,
              icon: const Icon(Icons.keyboard_arrow_down_sharp),
              elevation: 16,
              underline: const SizedBox(),
              onChanged: (String? value) {
                // This is called when the user selects an item.
                widget.callbackSubCat(value ?? "null");
                setState(() {
                  subDropDownValue = value!;
                });
              },
              isExpanded: true,
              items: mySubCategory.isEmpty
                  ? toDropDownItem(listSubCategory['Excluded']!)
                  : toDropDownItem(mySubCategory),
            ),
          ),
        )
      ],
    );
  }
}

// TODO: Isi Dari Alert
class ContentAlert extends StatelessWidget {
  const ContentAlert({super.key});

  @override
  Widget build(BuildContext context) {
    String merchantName = 'Transmart';
    toRupiah(int amount) {
      NumberFormat currencyFormatter = NumberFormat.currency(
        locale: 'id',
        symbol: 'Rp',
        decimalDigits: 0,
      );
      return currencyFormatter.format(amount);
    }

    return Container(
      height: 200,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        // border: Border.all(color: Colors.black, width: 2.0),
        borderRadius: BorderRadius.circular(10),
      ),
      padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
      alignment: Alignment.center,
      child: Consumer<DataDummy>(
        builder: (context, billData, child) {
          return Padding(
            padding: const EdgeInsets.all(10.0),
            child: Text(
              'Apakah anda yakin mengkonfirmasi ${billData.amount} item dengan total ${toRupiah(billData.total)} dari $merchantName?',
              textAlign: TextAlign.center,
              style: const TextStyle(fontSize: 16),
            ),
          );
        },
      ),
    );
  }
}
