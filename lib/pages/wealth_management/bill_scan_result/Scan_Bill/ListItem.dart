// ignore_for_file: prefer_const_constructors

import 'package:final_project_itdp/pages/wealth_management/bill_scan_result/DataDummy.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'Modal_Edit.dart';

class ListItem extends StatefulWidget {
  Trx trx;
  ListItem({required this.trx, super.key});

  @override
  State<ListItem> createState() => _ListItemState();
}

class _ListItemState extends State<ListItem> {
  bool value1 = false;
  @override
  Widget build(BuildContext context) {
    toRupiah(int amount) {
      NumberFormat currencyFormatter = NumberFormat.currency(
        locale: 'id',
        symbol: 'Rp',
        decimalDigits: 0,
      );
      return currencyFormatter.format(amount);
    }

    return Container(
      padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
            margin: const EdgeInsets.symmetric(horizontal: 10),
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey.shade300),
              color: Colors.white,
            ),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(0.0),
                      child: RichText(
                        text: TextSpan(
                          children: [
                            TextSpan(
                                text: widget.trx.subCat == ''
                                    ? widget.trx.cat
                                    : "${widget.trx.cat}-${widget.trx.subCat}",
                                style: const TextStyle(
                                  color: Colors.black,
                                  fontSize: 15,
                                )),
                            WidgetSpan(
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 2.0),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Row(
                      children: [
                        Text(
                          widget.trx.itemName,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                        ),
                        IconButton(
                          icon: Icon(
                            Icons.edit,
                            size: 15,
                            color: Colors.grey.shade600,
                          ),
                          onPressed: () {
                            MyModal.CategoryEditModal(
                                context: context, editTrx: widget.trx);
                          },
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 8.0),
                      child: Row(
                        children: [
                          Checkbox(
                            value: widget.trx.status,
                            onChanged: (bool? value) {
                              setState(() {
                                widget.trx.status = value!;
                              });
                            },
                          ),
                          const Text("Pilih"),
                        ],
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      toRupiah(widget.trx.price),
                      style: TextStyle(fontSize: 12, color: Colors.red),
                    ),
                    RichText(
                      text: TextSpan(
                        children: [
                          WidgetSpan(
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 2.0),
                              child: Text('${widget.trx.amount}x  '),
                            ),
                          ),
                          TextSpan(
                              text: toRupiah(
                                  widget.trx.amount * widget.trx.price),
                              style: const TextStyle(
                                color: Colors.black,
                                fontSize: 15,
                              )),
                        ],
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      RichText(
                        text: TextSpan(
                          children: const [
                            WidgetSpan(
                              child: Icon(
                                Icons.delete,
                                size: 15,
                              ),
                            ),
                            TextSpan(
                                text: " Diskon",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 12,
                                )),
                          ],
                        ),
                      ),
                      Text(
                        toRupiah(widget.trx.diskon),
                        style: TextStyle(color: Colors.green),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
