import 'package:final_project_itdp/pages/wealth_management/bill_scan_result/DataDummy.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'Modal_Edit.dart';

class ButtomResult extends StatefulWidget {
  ButtomResult({super.key});

  @override
  State<ButtomResult> createState() => _ButtomResultState();
}

class _ButtomResultState extends State<ButtomResult> {
  toRupiah(int amount) {
    NumberFormat currencyFormatter = NumberFormat.currency(
      locale: 'id',
      symbol: 'Rp',
      decimalDigits: 0,
    );
    return currencyFormatter.format(amount);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            FloatingActionButton.small(
              onPressed: () {
                MyModal.CategoryEditModal(context: context);
              },
              backgroundColor: const Color(0xFFFA9B23),
              child: const Icon(
                Icons.add,
                size: 30,
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 10,
        ),
        Consumer<DataDummy>(builder: (context, billData, child) {
          return Column(
            children: [
              //subtotal
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text('Subtotal'),
                  Text('${toRupiah(billData.subTotal)}')
                ],
              ),
              //diskon
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text('Diskon'),
                  Text(
                    '-${toRupiah(billData.diskon)}',
                    style: const TextStyle(color: Colors.green),
                  )
                ],
              ),
              //pajak
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text('Pajak'),
                  Text('${toRupiah(billData.pajak)}')
                ],
              ),
              //grand total
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text('Grand Total'),
                  Text(
                    '${toRupiah(billData.total)}',
                    style: const TextStyle(color: Colors.amber),
                  )
                ],
              ),
              const SizedBox(
                height: 25,
              ),
              ElevatedButton(
                onPressed: () {
                  MyModal.AlertBox(context);
                },
                style: ElevatedButton.styleFrom(
                  primary: const Color(0xFFFA9B23),
                  padding:
                      const EdgeInsets.symmetric(horizontal: 35, vertical: 15),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
                child: const Text('Konfirmasi Bill'),
              )
            ],
          );
        }),
      ],
    );
  }
}
