import 'package:final_project_itdp/pages/wealth_management/bill_scan_result/DataDummy.dart';
import 'package:final_project_itdp/pages/wealth_management/bill_scan_result/Scan_Bill/ButtomResult.dart';
import 'package:final_project_itdp/pages/wealth_management/bill_scan_result/Scan_Bill/ListItem.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:provider/provider.dart';

class ScanBill extends StatefulWidget {
  const ScanBill({super.key});

  @override
  State<ScanBill> createState() => _ScanBillState();
}

class _ScanBillState extends State<ScanBill> {
  TextEditingController edit = TextEditingController();
  final FocusNode unitCodeCtrlFocusNode = FocusNode();
  FocusNode focus = FocusNode();
  bool isEnable = true;
  bool value = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: const Color(0xFFFDC60C),
        elevation: 0.0,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(bottom: Radius.circular(15))),
        bottom: PreferredSize(
            preferredSize: const Size.fromHeight(10),
            child: Padding(
              padding: const EdgeInsets.only(left: 25, right: 25, bottom: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    height: 25,
                    width: 25,
                    child: TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      style: const ButtonStyle(
                        backgroundColor: MaterialStatePropertyAll(
                          Color(0xFFD9D9D9),
                        ),
                      ),
                      child: const Text("<"),
                    ),
                  ),
                  const Text(
                    "Scan Bill",
                    style: TextStyle(
                        fontSize: 18,
                        color: Colors.black,
                        fontWeight: FontWeight.w500),
                  ),
                  Container(
                    height: 25,
                    width: 25,
                  ),
                ],
              ),
            )),
      ),
      body: Consumer<DataDummy>(builder: (context, billData, child) {
        List<Trx> scanResult = billData.getData();
        return Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    // mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      const Text(
                        "Rincian Bill",
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      Text(
                        "Anda dapat merubah nama, jumlah, kategori, dan harga setiap item",
                        style: TextStyle(fontSize: 12, color: Colors.grey[410]),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        constraints:
                            const BoxConstraints(maxWidth: 100, minWidth: 0),
                        child: TextFormField(
                          decoration: const InputDecoration.collapsed(
                              hintText: 'Nama Merchant',
                              hintStyle: TextStyle(fontSize: 13),
                              border: InputBorder.none),
                          controller: edit,
                          focusNode: unitCodeCtrlFocusNode,
                          readOnly: isEnable,
                        ),
                      ),
                      IconButton(
                          icon: Icon(
                            Icons.edit,
                            size: 15,
                            color: Colors.grey.shade600,
                          ),
                          onPressed: () {
                            setState(() {
                              isEnable = !isEnable;
                              FocusScope.of(context)
                                  .requestFocus(unitCodeCtrlFocusNode);
                            });
                          }),
                    ],
                  ),
                  Row(
                    children: [
                      Checkbox(
                        value: value,
                        onChanged: (bool? value) {
                          setState(() {
                            this.value = value!;
                          });
                        },
                      ),
                      const Text("Pilih Semua"),
                    ],
                  ),
                ],
              ),
            ),
            Expanded(
                child: Container(
              padding: const EdgeInsets.symmetric(vertical: 10),
              color: const Color.fromARGB(255, 255, 255, 255),
              child: ListView.builder(
                itemCount: scanResult.length,
                itemBuilder: (context, index) {
                  return ListItem(trx: scanResult[index]);
                },
              ),
            )),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: ButtomResult(),
            ),
          ],
        );
      }),
    );
  }
}
