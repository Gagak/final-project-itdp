// ignore_for_file: unnecessary_this

import 'package:flutter/widgets.dart';

int myid = 0;

class DataDummy extends ChangeNotifier {
  List<Trx> scanResult = [];
  int subTotal = 0;
  int diskon = 0;
  int pajak = 0;
  int total = 0;
  int amount = 0;
  int id = 0;

  List<Trx> getData() {
    return scanResult;
  }

  void addBillTrx(Trx trx) {
    this.diskon += trx.diskon;
    subTotal += trx.amount * trx.price;
    this.total += this.subTotal - this.diskon;
    this.amount += trx.amount;
    scanResult.add(trx);

    notifyListeners();
  }

  void editBillTrx(int id, Trx trx) {
    int idx = this.scanResult.indexWhere((item) => item.id == id);
    scanResult[idx] = trx;
    setAll();
    notifyListeners();
  }

  void setAll() {
    int subTotal = 0;
    int diskon = 0;
    int total = 0;
    int amount = 0;
    for (var billTrx in scanResult) {
      subTotal += billTrx.amount * billTrx.price;
      diskon += billTrx.diskon;
      total += subTotal - billTrx.diskon;
      amount += billTrx.amount;
    }
    this.diskon = diskon;
    this.subTotal = subTotal;
    this.total = total;
    this.amount = amount;
  }

  void removeBillTrxdiskon(int id) {
    scanResult.removeWhere((item) => item.id == id);
  }
}

class Trx {
  int id = myid;
  String cat;
  String subCat;
  String itemName;
  bool status = true;
  int amount;
  int price;
  int diskon;
  Trx(this.cat, this.subCat, this.itemName, this.amount, this.diskon,
      this.price) {
    id = ++myid;
  }
}

// List<Map<String, dynamic>> scanResult = [
//     {
//       "cat": "Makanan",
//       "subCat": "",
//       "itemName": "Pao Salted Egg",
//       "status": true,
//       "amount": 2,
//       "price": 23000,
//       "diskon": 3000
//     },
//     {
//       "cat": "Makanan",
//       "subCat": "",
//       "itemName": "Fish Cake",
//       "status": true,
//       "amount": 1,
//       "price": 10000,
//       "diskon": 1000
//     },
//     {
//       "cat": "Minuman",
//       "subCat": "",
//       "itemName": "Fanta",
//       "status": true,
//       "amount": 2,
//       "price": 7000,
//       "diskon": 0
//     },
//     {
//       "cat": "Belanja",
//       "subCat": "Fashion",
//       "itemName": "3 Second",
//       "status": true,
//       "amount": 1,
//       "price": 1000000,
//       "diskon": 4000
//     },
//     {
//       "cat": "Belanja",
//       "subCat": "Fashion",
//       "itemName": "Vans",
//       "status": true,
//       "amount": 1,
//       "price": 20000000,
//       "diskon": 10000
//     },
//     {
//       "cat": "Belanja",
//       "subCat": "Gadget",
//       "itemName": "Iphone 15",
//       "status": true,
//       "amount": 1,
//       "price": 15000000,
//       "diskon": 1000
//     },
//   ];