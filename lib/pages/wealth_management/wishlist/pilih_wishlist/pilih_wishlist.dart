import 'package:carousel_slider/carousel_slider.dart';
import 'package:final_project_itdp/pages/wealth_management/wishlist/wishlist_input_edit/Add.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

class PilihWishlist extends StatefulWidget {
  const PilihWishlist({super.key});

  @override
  State<PilihWishlist> createState() => _PilihWishlistState();
}

class _PilihWishlistState extends State<PilihWishlist> {
  int change = 0;
  CarouselController controller = CarouselController();

  Container pilih(x) {
    if (x == 0) {
      return valas();
    } else if (x == 1) {
      return sbn();
    } else {
      return deposito();
    }
  }

  Container valas() {
    return Container(
      padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          const Text(
            'Valas',
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          ),
          const SizedBox(
            height: 5,
          ),
          Text(
            'Valuta asing atau sering disingkat dengan istilah valas merupakan suatu jenis perdagangan atau transaksi yang memperdagangkan mata uang suatu negara terhadap mata uang negara lainnya.',
            maxLines: 5,
            style: TextStyle(fontSize: 15, color: Colors.grey.shade600),
            textAlign: TextAlign.justify,
          ),
        ],
      ),
    );
  }

  Container sbn() {
    return Container(
      padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          const Text(
            'SBN',
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          ),
          const SizedBox(
            height: 5,
          ),
          Text(
            'Surat Berharga Negara atau yang lebih dikenal sebagai SBN adalah produk investasi yang diterbitkan dan dijamin oleh pemerintah Republik Indonesia.',
            maxLines: 5,
            style: TextStyle(fontSize: 15, color: Colors.grey.shade600),
            textAlign: TextAlign.justify,
          ),
        ],
      ),
    );
  }

  Container deposito() {
    return Container(
      padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          const Text(
            'Deposito',
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          ),
          const SizedBox(
            height: 5,
          ),
          Text(
            'Deposito adalah instrumen investasi yang dikeluarkan oleh perbankan dengan imbal hasil berupa suku bunga yang lebih tinggi daripada tabungan biasa.',
            maxLines: 7,
            style: TextStyle(fontSize: 15, color: Colors.grey.shade600),
            textAlign: TextAlign.justify,
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(0, 40, 0, 20),
          child: Container(
            height: MediaQuery.of(context).size.height,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  children: [
                    CarouselSlider(
                      options: CarouselOptions(
                          onPageChanged: (index, reason) {
                            setState(() {
                              change = index;
                              // controller.jumpToPage(index);
                            });
                            print(change);
                          },
                          viewportFraction: 0.58,
                          height: 300,
                          autoPlay: false,
                          enableInfiniteScroll: true,
                          enlargeCenterPage: true),
                      items: [
                        Container(
                            // margin: EdgeInsets.only(bottom: 10),
                            padding: EdgeInsets.all(20),
                            decoration: BoxDecoration(
                              border: Border.all(
                                  color: Colors.black.withOpacity(0.05)),
                              gradient: LinearGradient(
                                  begin: Alignment.topLeft,
                                  colors: [
                                    Colors.grey.withOpacity(0.5),
                                    Colors.grey.withOpacity(0.2),
                                    Colors.grey.withOpacity(0.1)
                                  ]),
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(20)),
                            ),
                            child: GestureDetector(
                              //PENCET BUAT MILIH
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => const AddWishList(),
                                    ));
                              },
                              child: FittedBox(
                                child: Column(
                                  children: [
                                    Container(
                                      // margin: EdgeInsets.only(top: 10),
                                      child: const Text(
                                        'Valas',
                                        style: TextStyle(
                                            fontSize: 23,
                                            fontWeight: FontWeight.bold,
                                            color: Color.fromARGB(
                                                255, 240, 145, 12)),
                                      ),
                                    ),
                                    const Image(
                                      image: AssetImage('images/valas.png'),
                                      height: 200,
                                    ),
                                  ],
                                ),
                              ),
                            )),
                        Container(
                            // margin: EdgeInsets.only(bottom: 20),
                            padding: const EdgeInsets.all(20),
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                  begin: Alignment.topLeft,
                                  colors: [
                                    Colors.grey.withOpacity(0.5),
                                    Colors.grey.withOpacity(0.2),
                                    Colors.grey.withOpacity(0.1)
                                  ]),
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(20)),
                            ),
                            child: GestureDetector(
                              //PENCET BUAT MILIH
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => const AddWishList(),
                                    ));
                              },
                              child: FittedBox(
                                child: Column(
                                  children: [
                                    Container(
                                      // margin: EdgeInsets.only(top: 10),
                                      child: const Text(
                                        'SBN',
                                        style: TextStyle(
                                            fontSize: 23,
                                            fontWeight: FontWeight.bold,
                                            color: Color.fromARGB(
                                                255, 240, 145, 12)),
                                      ),
                                    ),
                                    const Image(
                                      image: AssetImage('images/surat.png'),
                                      color: Color(0xFFF99D1C),
                                      height: 200,
                                    ),
                                  ],
                                ),
                              ),
                            )),
                        Container(
                            // margin: EdgeInsets.only(bottom: 20, top: 2),
                            padding: const EdgeInsets.all(20),
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                  begin: Alignment.topLeft,
                                  colors: [
                                    Colors.grey.withOpacity(0.5),
                                    Colors.grey.withOpacity(0.2),
                                    Colors.grey.withOpacity(0.1)
                                  ]),
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(20)),
                            ),
                            child: FittedBox(
                              child: GestureDetector(
                                //PENCET BUAT MILIH
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => const AddWishList(),
                                      ));
                                  print(change);
                                },
                                child: Column(
                                  children: [
                                    Container(
                                      // margin: EdgeInsets.only(top: 8),
                                      child: const Text(
                                        'Deposito',
                                        style: TextStyle(
                                            fontSize: 23,
                                            fontWeight: FontWeight.bold,
                                            color: Color.fromARGB(
                                                255, 240, 145, 12)),
                                      ),
                                    ),
                                    const Image(
                                      image: AssetImage('images/deposito.png'),
                                      color: Color(0xFFF99D1C),
                                      height: 200,
                                    ),
                                  ],
                                ),
                              ),
                            )),
                      ],
                    ),
                    Container(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: List.generate(
                        3,
                        (index) {
                          return Container(
                            margin: const EdgeInsets.only(right: 5),
                            alignment: Alignment.centerLeft,
                            height: 9,
                            width: 9,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: change == index
                                  ? const Color(0xFFF99D1C)
                                  : const Color.fromARGB(41, 0, 0, 0),
                            ),
                          );
                        },
                      ),
                    ),
                    Container(
                      height: 20,
                    ),
                    pilih(change),
                  ],
                ),
      
                // Container(
                //   height: 100,
                // ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Expanded(
                      child: Container(
                          margin: const EdgeInsets.only(right: 20, left: 20),
                          child: ElevatedButton(
                              style: const ButtonStyle(
                                  backgroundColor:
                                      MaterialStatePropertyAll(Color(0xFFF99D1C)),
                                  shape: MaterialStatePropertyAll(
                                      RoundedRectangleBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(20))))),
                              onPressed: () {},
                              child: Container(
                                padding: const EdgeInsets.all(5),
                                child: const Text(
                                  'Pilih',
                                  style: TextStyle(fontSize: 17),
                                ),
                              ))),
                    ),
                  ],
                )
              ],
            ),
          ),
          // Container(
          //   margin: EdgeInsets.only(bottom: 20),
          //   padding: EdgeInsets.fromLTRB(0, 0, 0, 20),
          //   decoration: BoxDecoration(
          //       color: Colors.white,
          //       borderRadius: BorderRadius.circular(20),
          //       boxShadow: [
          //         BoxShadow(
          //             color: Colors.black.withOpacity(0.2),
          //             spreadRadius: 2,
          //             blurRadius: 2)
          //       ]),
          //   child: Column(
          //     crossAxisAlignment: CrossAxisAlignment.center,
          //     children: [
          //       Row(
          //         children: [
          //           Expanded(
          //             child: Container(
          //                 decoration: BoxDecoration(
          //                   gradient: LinearGradient(
          //                       begin: Alignment.topLeft,
          //                       colors: [
          //                         Colors.grey.withOpacity(0.5),
          //                         Colors.grey.withOpacity(0.2),
          //                         Colors.grey.withOpacity(0.1)
          //                       ]),
          //                   borderRadius: BorderRadius.only(
          //                       topRight: Radius.circular(20),
          //                       topLeft: Radius.circular(20)),
          //                 ),
          //                 child: Image(
          //                   image: AssetImage('images/valas.png'),
          //                 )),
          //           ),
          //         ],
          //       ),
          //       Container(
          //         padding: EdgeInsets.fromLTRB(20, 20, 20, 0),
          //         child: Column(
          //           crossAxisAlignment: CrossAxisAlignment.stretch,
          //           children: [
          //             Text(
          //               'Valas',
          //               style: TextStyle(
          //                   fontSize: 20, fontWeight: FontWeight.bold),
          //             ),
          //             SizedBox(
          //               height: 5,
          //             ),
          //             Text(
          //               'Valuta asing atau sering disingkat dengan istilah valas merupakan suatu jenis perdagangan atau transaksi yang memperdagangkan mata uang suatu negara terhadap mata uang negara lainnya.',
          //               maxLines: 5,
          //               style: TextStyle(
          //                   fontSize: 12, color: Colors.grey.shade600),
          //               textAlign: TextAlign.justify,
          //             ),
          //           ],
          //         ),
          //       )
          //     ],
          //   ),
          // ),
          // Container(
          //   margin: EdgeInsets.only(bottom: 20),
          //   padding: EdgeInsets.fromLTRB(0, 0, 0, 20),
          //   decoration: BoxDecoration(
          //       color: Colors.white,
          //       borderRadius: BorderRadius.circular(20),
          //       boxShadow: [
          //         BoxShadow(
          //             color: Colors.black.withOpacity(0.2),
          //             spreadRadius: 2,
          //             blurRadius: 2)
          //       ]),
          //   child: Column(
          //     crossAxisAlignment: CrossAxisAlignment.center,
          //     children: [
          //       Row(
          //         children: [
          //           Expanded(
          //             child: Container(
          //                 decoration: BoxDecoration(
          //                   gradient: LinearGradient(
          //                       begin: Alignment.topLeft,
          //                       colors: [
          //                         Colors.grey.withOpacity(0.5),
          //                         Colors.grey.withOpacity(0.2),
          //                         Colors.grey.withOpacity(0.1)
          //                       ]),
          //                   borderRadius: BorderRadius.only(
          //                       topRight: Radius.circular(20),
          //                       topLeft: Radius.circular(20)),
          //                 ),
          //                 child: Image(
          //                   color: Color(0xFFF99D1C),
          //                   image: AssetImage('images/surat.png'),
          //                 )),
          //           ),
          //         ],
          //       ),
          //       Container(
          //         padding: EdgeInsets.fromLTRB(20, 20, 20, 0),
          //         child: Column(
          //           crossAxisAlignment: CrossAxisAlignment.stretch,
          //           children: [
          //             Text(
          //               'SBN',
          //               style: TextStyle(
          //                   fontSize: 20, fontWeight: FontWeight.bold),
          //             ),
          //             SizedBox(
          //               height: 5,
          //             ),
          //             Text(
          //               'Surat Berharga Negara atau yang lebih dikenal sebagai SBN adalah produk investasi yang diterbitkan dan dijamin oleh pemerintah Republik Indonesia.',
          //               maxLines: 5,
          //               style: TextStyle(
          //                   fontSize: 12, color: Colors.grey.shade600),
          //               textAlign: TextAlign.justify,
          //             ),
          //           ],
          //         ),
          //       )
          //     ],
          //   ),
          // ),
        ),
      ),
    );
  }
}
