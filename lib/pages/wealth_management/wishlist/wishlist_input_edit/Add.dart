import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:final_project_itdp/pages/wealth_management/wishlist/wishlist_input_edit/group_currency.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

class AddWishList extends StatefulWidget {
  const AddWishList({super.key});

  @override
  State<AddWishList> createState() => _AddWishListState();
}

class _AddWishListState extends State<AddWishList> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: const Color(0xFFFDC60C),
        elevation: 0.0,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(bottom: Radius.circular(15))),
        bottom: PreferredSize(
            preferredSize: const Size.fromHeight(10),
            child: Padding(
              padding: const EdgeInsets.only(left: 25, right: 25, bottom: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    height: 25,
                    width: 25,
                    child: TextButton(
                      onPressed: () {},
                      style: const ButtonStyle(
                        backgroundColor: MaterialStatePropertyAll(
                          Color(0xFFD9D9D9),
                        ),
                      ),
                      child: const Text("<"),
                    ),
                  ),
                  const Text(
                    "Add/Edit Wish List",
                    style: TextStyle(
                        fontSize: 18,
                        color: Colors.black,
                        fontWeight: FontWeight.w500),
                  ),
                  Container(
                    height: 25,
                    width: 25,
                  ),
                ],
              ),
            )),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(
            height: 20,
          ),
          const Center(
              child: Text(
            'Wish List VALAS ',
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
          )),
          const SizedBox(
            height: 20,
          ),
          const Center(
              child: Text(
            'Pilih Currency',
            style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500),
          )),
          const SizedBox(
            height: 40,
          ),
          const Center(child: Currency()),
          Center(
            child: Padding(
              padding: const EdgeInsets.all(55.0),
              child: Container(
                margin: const EdgeInsets.only(top: 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  // ignore: prefer_const_literals_to_create_immutables
                  children: [
                    const Text('InputYour Desired Amount'),
                    TextField(
                      decoration: const InputDecoration(
                        hintText: '(IDR) Rp0',
                      ),
                      inputFormatters: <TextInputFormatter>[
                        CurrencyTextInputFormatter(
                          locale: 'id',
                          decimalDigits: 0,
                          symbol: '(IDR) Rp',
                        ),
                      ],
                      keyboardType: TextInputType.number,
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    ClipRRect(
                      borderRadius: BorderRadius.circular(4),
                      child: Stack(
                        children: <Widget>[
                          Positioned.fill(
                            child: Container(
                              decoration: const BoxDecoration(
                                gradient: LinearGradient(
                                  colors: <Color>[
                                    Color.fromARGB(255, 236, 182, 4),
                                    Color.fromARGB(255, 230, 122, 0),
                                    Color.fromARGB(255, 236, 182, 4),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          TextButton(
                            style: TextButton.styleFrom(
                              foregroundColor: Colors.white,
                              padding: const EdgeInsets.all(16.0),
                              textStyle: const TextStyle(fontSize: 20),
                            ),
                            onPressed: () {},
                            child: const Text('IngatKan Saya'),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
