import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:group_button/group_button.dart';
//flutter pub add group_button

class Currency extends StatefulWidget {
  const Currency({super.key});

  @override
  State<Currency> createState() => _CurrencyState();
}

class _CurrencyState extends State<Currency> {
  @override
  Widget build(BuildContext context) {
    final controller = GroupButtonController();
    var checkbox = false;
    String selectedCurrecy = "";
    return Column(
      children: [
        GroupButton(
            options: const GroupButtonOptions(selectedColor: Colors.amber),
            controller: controller,
            buttons: const [
              "USD",
              "SGD",
              "AUD",
              "GBP",
              "CNY",
              "EUR",
              "JPY",
              "NZD",
              "HKD",
              "CHF"
            ],
            onSelected: (i, selected, checkbox) {
              debugPrint('Button $i $selected');
              selectedCurrecy = i;
            }),
      ],
    );
  }
}
