import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

class ListViewOfWishlist extends StatefulWidget {
  const ListViewOfWishlist({super.key});

  @override
  State<ListViewOfWishlist> createState() => _ListViewOfWishlistState();
}

class _ListViewOfWishlistState extends State<ListViewOfWishlist> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.fromLTRB(10, 10, 20, 10),
          margin: const EdgeInsets.all(10),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              border: Border.all(color: Colors.black, width: 1)),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Container(
                  width: 50,
                  height: 50,
                  child: Image.asset(
                    'assets/images/change.png',
                    height: 40,
                  ),
                ),
              ),
              Expanded(
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [const Text('Valas'), Text('Kurs Jual')],
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: const [
                        Text('(\$)  USD',
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 16)),
                        Text(
                          'Rp15.000',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 16),
                        )
                      ],
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        ElevatedButton(
                            style: const ButtonStyle(
                                backgroundColor:
                                    MaterialStatePropertyAll(Color(0xffD9D9D9)),
                                minimumSize:
                                    MaterialStatePropertyAll(Size(2, 25)),
                                alignment: Alignment.center),
                            onPressed: () {},
                            child: const Text(
                              'Hapus',
                              style:
                                  TextStyle(fontSize: 12, color: Colors.black),
                            )),
                        const SizedBox(
                          width: 7,
                        ),
                        ElevatedButton(
                            style: const ButtonStyle(
                                backgroundColor:
                                    MaterialStatePropertyAll(Color(0xffFDC60C)),
                                minimumSize:
                                    MaterialStatePropertyAll(Size(2, 25))),
                            onPressed: () {},
                            child: const Text(
                              'Ubah',
                              style:
                                  TextStyle(fontSize: 12, color: Colors.black),
                            ))
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
