import 'package:final_project_itdp/pages/wealth_management/wishlist/pilih_wishlist/pilih_wishlist.dart';

import '../../constants.dart';
import 'ListViewOfWishlist.dart';
import 'package:flutter/material.dart';

class WishlistHomePage extends StatelessWidget {
  const WishlistHomePage({super.key});
  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: DefaultTabController(
        length: 3,
        child: ViewPage(title: 'Wishlist'),
      ),
    );
  }
}

class ViewPage extends StatelessWidget {
  const ViewPage({super.key, required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: warnaAppBar,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(15),
          ),
        ),
        leading: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: const EdgeInsets.all(13.0),
              height: 25,
              width: 25,
              child: TextButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                style: backButtonStyle,
                child: const Text("<"),
              ),
            ),
          ],
        ),
        title: Text(title, style: const TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
        centerTitle: true,
        bottom: const TabBar(
          labelColor: Colors.black,
          unselectedLabelColor: Colors.black,
          tabs: [
            Tab(
              text: 'Wishlist',
            ),
            Tab(
              text: 'Tambah Wishlist',
            ),
          ],
        ),
      ),
      body: const TabBarView(
        children: [
          MyWishlistBody(),
          MenuWishlistBody(),
        ],
      ),
    );
  }
}

class MyWishlistBody extends StatelessWidget {
  const MyWishlistBody({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              margin: const EdgeInsets.all(10),
              child: const Text(
                'Wishlist Anda',
                style: TextStyle(fontSize: 23, fontWeight: FontWeight.bold),
              ),
            ),
            Expanded(
              child: ListView.builder(
                itemCount: 4,
                itemBuilder: (context, index) => ListViewOfWishlist(),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class MenuWishlistBody extends StatelessWidget {
  const MenuWishlistBody({super.key});

  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.all(8.0),
      child: Center(child: PilihWishlist()),
    );
  }
}
