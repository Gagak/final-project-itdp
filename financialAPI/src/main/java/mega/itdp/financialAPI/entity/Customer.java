package mega.itdp.financialAPI.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import mega.itdp.financialAPI.constants.GenderEnum;

@Entity
@Table(name = "customer")
public class Customer implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 553715391500260801L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Version
	private Long version;

	@NotNull
	private String customerId;
	
	@NotNull
	private String name;
	
	private GenderEnum gender;
	
	@Temporal(TemporalType.DATE)
	private Date birth_date;
	
	@NotNull
	private String address;
	
	@NotNull
	private String phoneNumber;
	
	@NotNull
	private String mothersMaidenName;
	
	@NotNull
	private String education;
	
	@Min(value = 0)
	private Double monthlyIncome;
	
	@Min(value = 0)
	private int familyMember;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdAt;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedAt;
	
	public void updateCustomer(Customer update) {
		this.name = update.getName();
		this.gender = update.getGender();
		this.birth_date = update.getBirth_date();
		this.address = update.getAddress();
		this.phoneNumber = update.getPhone_number();
		this.mothersMaidenName = update.getMothers_maiden_name();
		this.education = update.getEducation();
		this.monthlyIncome = update.getMonthly_income();
		this.familyMember = update.getFamily_member();
		this.modifiedAt = new Date();
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getCustomer_id() {
		return customerId;
	}

	public void setCustomer_id(String customer_id) {
		this.customerId = customer_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public GenderEnum getGender() {
		return gender;
	}

	public void setGender(GenderEnum gender) {
		this.gender = gender;
	}

	public Date getBirth_date() {
		return birth_date;
	}

	public void setBirth_date(Date birth_date) {
		this.birth_date = birth_date;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone_number() {
		return phoneNumber;
	}

	public void setPhone_number(String phone_number) {
		this.phoneNumber = phone_number;
	}

	public String getMothers_maiden_name() {
		return mothersMaidenName;
	}

	public void setMothers_maiden_name(String mothers_maiden_name) {
		this.mothersMaidenName = mothers_maiden_name;
	}

	public String getEducation() {
		return education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public Double getMonthly_income() {
		return monthlyIncome;
	}

	public void setMonthly_income(Double monthly_income) {
		this.monthlyIncome = monthly_income;
	}

	public int getFamily_member() {
		return familyMember;
	}

	public void setFamily_member(int family_member) {
		this.familyMember = family_member;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getModifiedAt() {
		return modifiedAt;
	}

	public void setModifiedAt(Date modifiedAt) {
		this.modifiedAt = modifiedAt;
	}
}
