package mega.itdp.financialAPI.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.Min;

import mega.itdp.financialAPI.result.TransactionDetailsResult;

@Entity
@Table(name = "transaction_details")
public class TransactionDetails implements Serializable
{
	/**
	 *
	 */
	private static final long serialVersionUID = -5855878461411282711L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Version
	private Long version;
	
	private String itemName;
	
	@Min(value = 0)
	private int itemQuantity;
	
	@Min(value = 0)
	private Double price;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdAt;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedAt;
	
	@ManyToOne
	private BillTransaction billTransaction;
	
	@ManyToOne
	private TransactionSubCategory transactionSubCategory;
	
	public void updateFields (TransactionDetailsResult updates) 
	{
		this.itemName = updates.getItemName();
		this.itemQuantity = updates.getItemQuantity();
		this.price = updates.getPrice();
		this.modifiedAt = new Date();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public int getItemQuantity() {
		return itemQuantity;
	}

	public void setItemQuantity(int itemQuantity) {
		this.itemQuantity = itemQuantity;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getModifiedAt() {
		return modifiedAt;
	}

	public void setModifiedAt(Date modifiedAt) {
		this.modifiedAt = modifiedAt;
	}

	public BillTransaction getBillTransaction() {
		return billTransaction;
	}

	public void setBillTransaction(BillTransaction billTransaction) {
		this.billTransaction = billTransaction;
	}

	public TransactionSubCategory getTransactionSubCategory() {
		return transactionSubCategory;
	}

	public void setTransactionSubCategory(TransactionSubCategory transactionSubCategory) {
		this.transactionSubCategory = transactionSubCategory;
	}
}
