package mega.itdp.financialAPI.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import mega.itdp.financialAPI.entity.TransactionCategory;

public interface TransactionCategoryRepository extends JpaRepository<TransactionCategory, Long>{
	List<TransactionCategory> findAllByCategoryId(String categoryId);
	
	@Query(value = "SELECT DISTINCT category.* FROM transaction_category AS category "
			+ "JOIN transaction_sub_category AS sub ON sub.transaction_category_id = category.id "
			+ "JOIN transaction_details AS details ON details.transaction_sub_category_id = sub.id "	
			+ "JOIN bill_transaction AS bill ON details.bill_transaction_id = bill.id "			
			+ "WHERE bill.id = :billId", nativeQuery = true)
	List<TransactionCategory> findAllByBillId(@Param("billId") Long billId);
}
