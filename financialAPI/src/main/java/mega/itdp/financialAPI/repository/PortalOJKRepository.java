package mega.itdp.financialAPI.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import mega.itdp.financialAPI.entity.PortalOJK;

public interface PortalOJKRepository extends JpaRepository<PortalOJK, Long>{

}
