package mega.itdp.financialAPI.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import mega.itdp.financialAPI.entity.BillTransaction;

public interface BillTransactionRepository extends JpaRepository<BillTransaction, Long>{
	List<BillTransaction> findAllByCustomerId(Long id);
	List<BillTransaction> findAllByTransactionTypeId(Long id);
	
	@Query(value = "SELECT b.id, b.version, b.transaction_date, b.merchant_name, b.total_price, b.created_at, b.modified_at FROM bill_transaction b "
			+ "JOIN transaction_details d ON d.bill_transaction_id = b.id "
			+ "JOIN transaction_sub_category s ON d.transaction_sub_category_id = s.id "
			+ "JOIN transaction_category c ON s.transaction_category_id = c.id "
			+ "WHERE c.id = :categoryId", nativeQuery = true)
	List<BillTransaction> findAllByCategoryId(@Param("categoryId") Long categoryId);
	
	@Query(value = "SELECT COUNT(details.id) FROM bill_transaction as bill "
			+ "JOIN transaction_details AS details ON details.bill_transaction_id = bill.id "
			+ "WHERE bill.id = :billId", nativeQuery = true)
	int findItemCount(@Param("billId") Long billId);
	
	@Query(value = "SELECT COUNT(DISTINCT category.id) FROM bill_transaction as bill "
			+ "JOIN transaction_details AS details ON details.bill_transaction_id = bill.id "
			+ "JOIN transaction_sub_category AS sub_category ON sub_category.id = details.transaction_sub_category_id "
			+ "JOIN transaction_category AS category ON category.id = sub_category.transaction_category_id "
			+ "WHERE bill.id = :billId", nativeQuery = true)	
	int findCategoryCount(@Param("billId") Long billId);
}
