package mega.itdp.financialAPI.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import mega.itdp.financialAPI.entity.TransactionSubCategory;

public interface TransactionSubCategoryRepository extends JpaRepository<TransactionSubCategory, Long>{
	List<TransactionSubCategory> findAllBySubCategoryId(String subCategoryId);
	List<TransactionSubCategory> findAllByTransactionCategoryId(Long id);
}
