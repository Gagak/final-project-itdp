package mega.itdp.financialAPI.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import mega.itdp.financialAPI.entity.CustomerAccount;

public interface CustomerAccountRepository extends JpaRepository<CustomerAccount, Long> {
	List<CustomerAccount> findAllByCustomerId(Long id);
}
