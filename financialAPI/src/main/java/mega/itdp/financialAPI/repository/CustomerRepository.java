package mega.itdp.financialAPI.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import mega.itdp.financialAPI.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long>{
	Customer findByCustomerId(String id);
}
