package mega.itdp.financialAPI.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import mega.itdp.financialAPI.entity.TransactionDetails;

public interface TransactionDetailsRepository extends JpaRepository<TransactionDetails, Long>
{
	List<TransactionDetails> findAllByBillTransactionId(Long id);
	List<TransactionDetails> findAllByTransactionSubCategoryId(Long id);
	
	@Query(value = "SELECT d.* FROM transaction_details d "
			+ "JOIN transaction_sub_category s ON d.transaction_sub_category_id = s.id "
			+ "JOIN transaction_category c ON s.transaction_category_id = c.id "
			+ "WHERE c.id = :categoryId", nativeQuery = true)
	List<TransactionDetails> findAllByCategoryId(@Param("categoryId") Long categoryId);
	
	@Query(value = "SELECT d.* FROM transaction_details d "
			+ "JOIN bill_transaction b ON d.bill_transaction_id = b.id "
			+ "JOIN transaction_sub_category s ON d.transaction_sub_category_id = s.id "
			+ "JOIN transaction_category c ON s.transaction_category_id = c.id "
			+ "WHERE b.transaction_date BETWEEN :startDate AND :endDate", nativeQuery = true)
	List<TransactionDetails> findAllByDateRange(@Param("startDate") Date startDate, @Param("endDate") Date endDate);
	
	@Query(value = "SELECT d.* FROM transaction_details d "
			+ "JOIN bill_transaction b ON d.bill_transaction_id = b.id "
			+ "JOIN transaction_sub_category s ON d.transaction_sub_category_id = s.id "
			+ "JOIN transaction_category c ON s.transaction_category_id = c.id "
			+ "WHERE b.transaction_date BETWEEN :startDate AND :endDate AND c.id = :categoryId", nativeQuery = true)
	List<TransactionDetails> findAllByDateRangeAndCategoryId(@Param("startDate") Date startDate, @Param("endDate") Date endDate, @Param("categoryId") Long categoryId);
}
