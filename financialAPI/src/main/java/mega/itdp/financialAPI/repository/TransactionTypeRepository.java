package mega.itdp.financialAPI.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import mega.itdp.financialAPI.entity.TransactionType;

public interface TransactionTypeRepository extends JpaRepository<TransactionType, Long> {

}
