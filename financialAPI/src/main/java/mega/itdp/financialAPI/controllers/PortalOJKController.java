package mega.itdp.financialAPI.controllers;

import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mega.itdp.financialAPI.entity.PortalOJK;
import mega.itdp.financialAPI.repository.PortalOJKRepository;

@RestController
@RequestMapping("/portalOJK")
public class PortalOJKController {
	
	private final Log log = LogFactory.getLog(PortalOJKController.class);
	
	@Autowired
	PortalOJKRepository portalOJKRepository;
	
	
	@GetMapping
	public List<PortalOJK> getAllPortalOJK(){
		log.info("LOG: GET ALL PORTAL OJK");
		return portalOJKRepository.findAll();
	}
	
	@GetMapping("/{portalOJKId}")
	public PortalOJK getOnePortalOJK(@PathVariable Long portalOJKId){
		log.info("LOG: GET PORTAL OJK BY ID: " + portalOJKId);
		return portalOJKRepository.findById(portalOJKId).get();
	}
	
	@PostMapping
	public PortalOJK saveOnePortalOJK(@RequestBody PortalOJK portalOJK) {
		log.info("LOG: ADD NEW PORTAL OJK");
		portalOJK.setCreatedAt(new Date());
		PortalOJK inserted = portalOJKRepository.save(portalOJK);
		log.info("LOG: NEW BILL ID: " + inserted.getId());
		return inserted;
	}
	
	@PutMapping("/{portalOJKId}")
	public PortalOJK updateOneBill(@RequestBody PortalOJK portalOJKUpdates, @PathVariable Long portalOJKId) {
		log.info("LOG: UPDATE PORTAL OJK WITH ID: " + portalOJKId);
		PortalOJK existing = portalOJKRepository.findById(portalOJKId).get();
		existing.updateFields(portalOJKUpdates);
		
		return portalOJKRepository.save(existing);
	}
	
	@DeleteMapping("/{portalOJKId}")
	public void deleteOneBill(@PathVariable Long portalOJKId) {
		log.info("LOG: DELETE BILL WITH ID: " + portalOJKId);
		PortalOJK toDelete = portalOJKRepository.findById(portalOJKId).get();
		portalOJKRepository.delete(toDelete);
	}
	
}
