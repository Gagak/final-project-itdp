package mega.itdp.financialAPI.controllers;

import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mega.itdp.financialAPI.entity.Customer;
import mega.itdp.financialAPI.repository.CustomerRepository;

@RestController
@RequestMapping(value = "nasabah")
public class CustomerController
{
	private final Log log = LogFactory.getLog(CustomerController.class);
	
	@Autowired
	private CustomerRepository customerRepository;
	
	//	Get All Nasabah
	@GetMapping
	public List<Customer> getAllCustomer()
	{
		log.info("LOG: GET ALL CUSTOMER");
		return customerRepository.findAll();
	}
	
	//	Get One Nasabah
	@GetMapping("/{customerId}")
	public Customer getOneCustomer(@PathVariable Long customerId) {
		log.info("LOG: GET CUSTOMER WITH ID: " + customerId);
		Customer customer = customerRepository.findById(customerId).get();
		return customer;
	}
	
	//	Add Nasabah
	@PostMapping
	public Customer addOneCustomer(@RequestBody Customer customer) {
		log.info("LOG: ADD NEW CUSTOMER");
		customer.setCreatedAt(new Date());
		log.info("LOG: NEW CUSTOMER ID: " + customer.getId());
		return customerRepository.save(customer);
	}
	
	// Edit Nasabah
	@PutMapping("/{customerId}")
	public Customer editOneCustomer(@PathVariable Long customerId, @RequestBody Customer customerUpdate) {
		log.info("LOG: UPDATE CUSTOMER WITH ID: " + customerId);
		Customer existing = customerRepository.findById(customerId).get();
		existing.updateCustomer(customerUpdate);
		
		Customer updatedCustomer = customerRepository.save(existing);
		
		return updatedCustomer;
	}
	
	// Delete Nasabah
	@DeleteMapping("/{customerId}")
	public void deleteOneCustomer(@PathVariable Long customerId)
	{
		log.info("LOG: DELETE CUSTOMER WITH ID: " + customerId);
		Customer customerToDelete = customerRepository.findById(customerId).get();
		customerRepository.delete(customerToDelete);
	}
}
