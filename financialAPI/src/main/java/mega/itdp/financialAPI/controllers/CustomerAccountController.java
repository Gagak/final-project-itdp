package mega.itdp.financialAPI.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mega.itdp.financialAPI.entity.Customer;
import mega.itdp.financialAPI.entity.CustomerAccount;
import mega.itdp.financialAPI.repository.CustomerAccountRepository;
import mega.itdp.financialAPI.repository.CustomerRepository;
import mega.itdp.financialAPI.result.CustomerAccountResult;

@RestController
@RequestMapping(value = "accountNasabah")
public class CustomerAccountController
{
	private final Log log = LogFactory.getLog(CustomerAccountController.class);

	@Autowired
	private CustomerAccountRepository customerAccountRepository;
	@Autowired
	private CustomerRepository customerRepository;
	
	// Get All Customer Account
	@GetMapping
	public List<CustomerAccountResult> getAllCustomerAccount(){
		List<CustomerAccount> selectAll = customerAccountRepository.findAll();
		List<CustomerAccountResult> results = new ArrayList<>();

		for (CustomerAccount account : selectAll) {
			results.add(new CustomerAccountResult(account));
		}

		
		log.info("LOG: GET ALL CUSTOMER ACCOUNT");
		return results;
	}
	
	// Get One Customer Account
	@GetMapping("/{customerAccountId}")
	public CustomerAccountResult getOneCustomerAccount(@PathVariable Long customerAccountId){
		CustomerAccount selectOne = customerAccountRepository.findById(customerAccountId).get();
		CustomerAccountResult result = new CustomerAccountResult(selectOne);
		
		log.info("LOG: GET CUSTOMER ACCOUNT WITH ID: " + customerAccountId);
		return result;
	}
	
	// Add Customer Account
	@PostMapping
	public CustomerAccountResult saveOneCustomerAccount(@RequestBody CustomerAccountResult customerAccountResult) {
		log.info("LOG: ADD NEW CUSTOMER ACCOUNT");
		Customer customer = null;
		if (customerAccountResult.getCustomerId() != null) {
			customer = customerRepository.findById(customerAccountResult.getCustomerId()).get();
		}
		CustomerAccount customerAccount = customerAccountResult.backToEntity(customerAccountResult);
		customerAccount.setCustomer(customer);
		customerAccount.setCreatedAt(new Date());
		log.info("LOG: NEW CUSTOMER ACCOUNT ID: " + customerAccount.getId());
		return new CustomerAccountResult(customerAccountRepository.save(customerAccount));
	}
	
	// Edit Customer Account
	@PutMapping("/{customerAccountId}")
	public CustomerAccountResult updateOneCustomerAccount(@RequestBody CustomerAccountResult customerAccountUpdates, @PathVariable Long customerAccountId) {
		log.info("LOG: UPDATE CUSTOMER ACCOUNT WITH ID: " + customerAccountId);
		Customer customer = null;
		if (customerAccountUpdates.getCustomerId() != null) {
			customer = customerRepository.findById(customerAccountUpdates.getCustomerId()).get();
		}
		CustomerAccount existing = customerAccountRepository.findById(customerAccountId).get();
		
		existing.setVersion(customerAccountUpdates.getVersion());
		existing.updateFields(customerAccountUpdates);
		existing.setCustomer(customer);
		
		return new CustomerAccountResult(customerAccountRepository.save(existing));
	}
	
	// Delete Customer Account
	@DeleteMapping("/{customerAccountId}")
	public void deleteOneCustomerAccount(@PathVariable Long customerAccountId) {
		log.info("LOG: DELETE CUSTOMER ACCOUNT WITH ID: " + customerAccountId);
		CustomerAccount toDelete = customerAccountRepository.findById(customerAccountId).get();
		customerAccountRepository.delete(toDelete);
	}
}
