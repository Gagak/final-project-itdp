package mega.itdp.financialAPI.controllers;

import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mega.itdp.financialAPI.entity.TransactionCategory;
import mega.itdp.financialAPI.repository.TransactionCategoryRepository;

@RestController
@RequestMapping(value = "kategoriTransaksi")
public class TransactionCategoryController
{
	private final Log log = LogFactory.getLog(TransactionCategoryController.class);
	
	@Autowired
	private TransactionCategoryRepository transactionCategoryRepository;
	
	// Get All Transaction Category
	@GetMapping
	public List<TransactionCategory> getAllransactionCategory()
	{	
		log.info("LOG: GET ALL TRANSACTION CATEGORY");
		return transactionCategoryRepository.findAll();
	}
	
	// Get One Transaction Category
	@GetMapping("/{categoryId}")
	public TransactionCategory getOneTransactionCategory(@PathVariable Long categoryId)
	{
		TransactionCategory selectOne = transactionCategoryRepository.findById(categoryId).get();
		
		log.info("LOG: GET ONE TRANSACTION CATEGORY");
		return selectOne;
	}
	
	// Add One Transaction Category
	@PostMapping
	public TransactionCategory saveOneTransactionCategory(@RequestBody TransactionCategory transactionCategory)
	{
		log.info("LOG: ADD NEW TRANSACTION CATEGORY");
		transactionCategory.setCreatedAt(new Date());
		
		log.info("LOG: NEW TRANSACTION CATEGORY ID: " + transactionCategory.getId());
		return transactionCategoryRepository.save(transactionCategory);
	}
	
	// Edit One Transaction Category
	@PutMapping("/{categoryId}")
	public TransactionCategory editOneTransactionCategory(@RequestBody TransactionCategory transactionCategoryUpdates, @PathVariable Long categoryId)
	{
		log.info("LOG: UPDATE TRANSACTION CATEGORY WITH ID: " + categoryId);
		TransactionCategory existing = transactionCategoryRepository.findById(categoryId).get();
		existing.updateFields(transactionCategoryUpdates);
		
		return transactionCategoryRepository.save(existing);
	}
	
	// Delete One Transaction Category
	@DeleteMapping("/{categoryId}")
	public void deleteOneTransactionCategory(@PathVariable Long categoryId)
	{
		TransactionCategory toDelete = transactionCategoryRepository.findById(categoryId).get();
		transactionCategoryRepository.delete(toDelete);
		log.info("LOG: DELETE TRANSACTION CATEGORY WITH ID: " + categoryId);
	}
}
