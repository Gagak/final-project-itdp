package mega.itdp.financialAPI.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mega.itdp.financialAPI.entity.BillTransaction;
import mega.itdp.financialAPI.entity.Customer;
import mega.itdp.financialAPI.entity.TransactionCategory;
import mega.itdp.financialAPI.entity.TransactionDetails;
import mega.itdp.financialAPI.entity.TransactionType;
import mega.itdp.financialAPI.repository.BillTransactionRepository;
import mega.itdp.financialAPI.repository.CustomerRepository;
import mega.itdp.financialAPI.repository.TransactionCategoryRepository;
import mega.itdp.financialAPI.repository.TransactionDetailsRepository;
import mega.itdp.financialAPI.repository.TransactionTypeRepository;
import mega.itdp.financialAPI.result.BillTransactionDetailResult;
import mega.itdp.financialAPI.result.BillTransactionResult;
import mega.itdp.financialAPI.result.ListBillTransactionResult;

@RestController
@RequestMapping(value = "transaksi&Bill")
public class BillTransactionController
{
	private final Log log = LogFactory.getLog(BillTransactionController.class);
	
	@Autowired
	private BillTransactionRepository billTransactionRepository;
	@Autowired
	private CustomerRepository customerRepository;
	@Autowired
	private TransactionTypeRepository transactionTypeRepository;
	@Autowired
	private TransactionCategoryRepository transactionCategoryRepository;
	@Autowired
	private TransactionDetailsRepository transactionDetailsRepository;
	
	// Get All Bill Transaction
	@GetMapping
	public List<ListBillTransactionResult> getAllBillTransaction()
	{
		log.info("LOG: GET ALL BILL TRANSACTION");
		List<BillTransaction> selectAll = billTransactionRepository.findAll();
		List<ListBillTransactionResult> results = new ArrayList<>();
		
		// Looping per bill
		for (BillTransaction billTransaction : selectAll)
		{
			ListBillTransactionResult transactionResult = new ListBillTransactionResult(billTransaction);
			
			// Dapetin berapa banyak transaction details
			transactionResult.setTotalItem(billTransactionRepository.findItemCount(billTransaction.getId()));
			
			// Dapetin berapa banyak transaction category
			transactionResult.setTotalCategory(billTransactionRepository.findCategoryCount(billTransaction.getId()));
			results.add(transactionResult);
		}
		
		return results;
	}
	
	// Get All Bill Transaction By CategoryId
	@GetMapping("/categoryId={categoryId}")
	public List<BillTransaction> getAllBillTransactionByCategoryId(@PathVariable Long categoryId)
	{
		log.info("LOG: GET ALL BILL TRANSACTION BY CATEGORY ID : " + categoryId);
		return billTransactionRepository.findAllByCategoryId(categoryId);
	}
	
	// Get One Bill Transaction
	@GetMapping("/{billTransactionId}")
	public BillTransactionDetailResult getOneBillTransaction(@PathVariable Long billTransactionId){
		BillTransaction selectOne = billTransactionRepository.findById(billTransactionId).get();
		BillTransactionDetailResult result = new BillTransactionDetailResult(selectOne);
		
		// Dapetin category apa aja berdasarkan bill Id
		List<TransactionCategory> categoryByBill = transactionCategoryRepository.findAllByBillId(billTransactionId);
		log.info("LOG: CATEGORY BY BILL LENGTH: " + categoryByBill.size());
		result.setCategoryByBill(categoryByBill);
		
		// Dapetin list detail berdasarkan category Id yang berdasarkan bill Id
		List<List<TransactionDetails>> listDetails = new ArrayList<>();
		for (TransactionCategory category : categoryByBill) {
			List<TransactionDetails> innerDetails = transactionDetailsRepository.findAllByCategoryId(category.getId());
			listDetails.add(innerDetails);
		}
		log.info("LOG: TRANSACTION DETAILS LENGTH: " + listDetails.size());
		result.setDetails(listDetails);
		
		log.info("LOG: GET BILL TRANSACTION WITH ID: " + billTransactionId);
		return result;
	}
	
	// Add One Bill Transaction
	@PostMapping
	public BillTransactionResult saveOneBillTransaction(@RequestBody BillTransactionResult billTransactionResult) {
		log.info("LOG: ADD NEW CUSTOMER ACCOUNT");
		Customer customer = null;
		TransactionType transactionType = null;
		if (billTransactionResult.getCustomerId() != null && billTransactionResult.getTransactionTypeId() != null) {
			customer = customerRepository.findById(billTransactionResult.getCustomerId()).get();
			transactionType = transactionTypeRepository.findById(billTransactionResult.getTransactionTypeId()).get();
		}
		BillTransaction billTransaction = billTransactionResult.backToEntity(billTransactionResult);
		billTransaction.setCustomer(customer);
		billTransaction.setTransactionType(transactionType);
		billTransaction.setCreatedAt(new Date());
		log.info("LOG: NEW BILL TRANSACTION ID: " + billTransaction.getId());
		return new BillTransactionResult(billTransactionRepository.save(billTransaction));
	}
	
	// Edit One Bill Transaction
	@PutMapping("/{billTransactionId}")
	public BillTransactionResult updateOneBillTransaction(@RequestBody BillTransactionResult billTransactionUpdates, @PathVariable Long billTransactionId) {
		log.info("LOG: UPDATE CUSTOMER ACCOUNT WITH ID: " + billTransactionId);
		Customer customer = null;
		TransactionType transactionType = null;
		if (billTransactionUpdates.getCustomerId() != null && billTransactionUpdates.getTransactionTypeId() != null) {
			customer = customerRepository.findById(billTransactionUpdates.getCustomerId()).get();
			transactionType = transactionTypeRepository.findById(billTransactionUpdates.getTransactionTypeId()).get();
		}
		BillTransaction existing = billTransactionRepository.findById(billTransactionId).get();
		
		existing.setId(billTransactionUpdates.getId());
		existing.setVersion(billTransactionUpdates.getVersion());
		existing.updateFields(billTransactionUpdates);
		existing.setCustomer(customer);
		existing.setTransactionType(transactionType);
		
		return new BillTransactionResult(billTransactionRepository.save(existing));
	}
	
	// Delete One Bill Transaction
	@DeleteMapping("/{billtransactionid}")
	public void deleteOneBillTransaction(@PathVariable Long billtransactionid) {
		log.info("LOG: DELETE CUSTOMER ACCOUNT WITH ID: " + billtransactionid);
		BillTransaction toDelete = billTransactionRepository.findById(billtransactionid).get();
		billTransactionRepository.delete(toDelete);
	}
}
