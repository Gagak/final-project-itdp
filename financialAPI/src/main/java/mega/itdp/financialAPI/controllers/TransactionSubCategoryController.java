package mega.itdp.financialAPI.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mega.itdp.financialAPI.entity.TransactionCategory;
import mega.itdp.financialAPI.entity.TransactionSubCategory;
import mega.itdp.financialAPI.repository.TransactionCategoryRepository;
import mega.itdp.financialAPI.repository.TransactionSubCategoryRepository;
import mega.itdp.financialAPI.result.TransactionSubCategoryResult;

@RestController
@RequestMapping(value = "subKategoriTransaksi")
public class TransactionSubCategoryController
{
	private final Log log = LogFactory.getLog(TransactionSubCategoryController.class);
	
	@Autowired
	private TransactionSubCategoryRepository transactionSubCategoryRepository;
	@Autowired
	private TransactionCategoryRepository transactionCategoryRepository;
	
	// Get All Sub Category
	@GetMapping
	public List<TransactionSubCategoryResult> getAllransactionSubCategory()
	{
		List<TransactionSubCategory> selectAll = transactionSubCategoryRepository.findAll();
		List<TransactionSubCategoryResult> results = new ArrayList<>();
		
		for (TransactionSubCategory subcategory : selectAll) {
			results.add(new TransactionSubCategoryResult(subcategory));
		}
		
		log.info("LOG: GET ALL TRANSACTION SUB CATEGORY");
		return results;
	}
	
	// Get All Sub category By Category Id
	@GetMapping("/categoryId={categoryId}")
	public List<TransactionSubCategoryResult> getAllransactionSubCategoryByCategoryId(@PathVariable Long categoryId)
	{
		List<TransactionSubCategory> selectByCategoryId = transactionSubCategoryRepository.findAllByTransactionCategoryId(categoryId);
		List<TransactionSubCategoryResult> results = new ArrayList<>();
		
		for (TransactionSubCategory subcategory : selectByCategoryId) {
			results.add(new TransactionSubCategoryResult(subcategory));
		}
		
		log.info("LOG: GET ALL TRANSACTION SUB CATEGORY BY CATEGORY ID");
		return results;
	}
	
	// Get One Sub Category
	@GetMapping("/{subCategoryId}")
	public TransactionSubCategoryResult getOneTransactionSubCategory(@PathVariable Long subCategoryId)
	{
		TransactionSubCategory selectOne = transactionSubCategoryRepository.findById(subCategoryId).get();
		TransactionSubCategoryResult result = new TransactionSubCategoryResult(selectOne);
		
		log.info("LOG: GET ONE TRANSACTION SUB CATEGORY");
		return result;
	}
	
	// Add One Sub Category
	@PostMapping
	public TransactionSubCategoryResult saveOneTransactionSubCategory(@RequestBody TransactionSubCategoryResult transactionSubCategoryResult)
	{
		log.info("LOG: ADD NEW TRANSACTION SUB CATEGORY");
		TransactionCategory category = null;
		if (transactionSubCategoryResult.getTransactionCategoryId() != null) {
			category = transactionCategoryRepository.findById(transactionSubCategoryResult.getTransactionCategoryId()).get();
		}
		TransactionSubCategory transactionSubCategory = transactionSubCategoryResult.backToEntity(transactionSubCategoryResult);
		transactionSubCategory.setTransactionCategory(category);
		transactionSubCategory.setCreatedAt(new Date());
		
		log.info("LOG: NEW TRANSACTION SUB CATEGORY ID: " + transactionSubCategory.getId());
		return new TransactionSubCategoryResult(transactionSubCategoryRepository.save(transactionSubCategory));
	}
	
	// Edit One Sub Category
	@PutMapping("/{subCategoryId}")
	public TransactionSubCategoryResult editOneTransactionSubCategory(@RequestBody TransactionSubCategoryResult transactionSubCategoryUpdates, @PathVariable Long subCategoryId)
	{
		log.info("LOG: UPDATE TRANSACTION SUB CATEGORY WITH ID: " + subCategoryId);
		TransactionCategory category = null;
		if (transactionSubCategoryUpdates.getTransactionCategoryId() != null) {
			category = transactionCategoryRepository.findById(transactionSubCategoryUpdates.getTransactionCategoryId()).get();
		}
		TransactionSubCategory existing = transactionSubCategoryRepository.findById(subCategoryId).get();
		
		existing.setVersion(transactionSubCategoryUpdates.getVersion());
		existing.updateFields(transactionSubCategoryUpdates);
		existing.setModifiedAt(new Date());
		existing.setTransactionCategory(category);
		
		return new TransactionSubCategoryResult(transactionSubCategoryRepository.save(existing));
	}
	
	// Delete One Sub Category
	@DeleteMapping("/{categoryId}")
	public void deleteOneTransactionSubCategory(@PathVariable Long subCategoryId)
	{
		TransactionSubCategory toDelete = transactionSubCategoryRepository.findById(subCategoryId).get();
		transactionSubCategoryRepository.delete(toDelete);
		log.info("LOG: DELETE TRANSACTION SUB CATEGORY WITH ID: " + subCategoryId);
	}
}
