package mega.itdp.financialAPI.controllers;

import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mega.itdp.financialAPI.entity.TransactionType;
import mega.itdp.financialAPI.repository.TransactionTypeRepository;

@RestController
@RequestMapping(value = "tipeTransaksi")
public class TransactionTypeController
{
	private final Log log = LogFactory.getLog(TransactionTypeController.class);
	
	@Autowired
	private TransactionTypeRepository transactionTypeRepository;
	
	// Get All Transaction Type
	@GetMapping
	public List<TransactionType> getAllTransactionType()
	{
		log.info("LOG: GET ALL TRANSACTION TYPE");
		return transactionTypeRepository.findAll();
	}
	
	//	Get One Transaction Type
	@GetMapping("/{transactionTypeId}")
	public TransactionType getOneTransactionType(@PathVariable Long transactionTypeId) {
		log.info("LOG: GET CUSTOMER WITH ID: " + transactionTypeId);
		TransactionType transactionType = transactionTypeRepository.findById(transactionTypeId).get();
		return transactionType;
	}
	
	// Add Transaction Type
	@PostMapping
	public TransactionType saveOneTransactionType(@RequestBody TransactionType transactiontype)
	{
		log.info("LOG: ADD NEW TRANSACTION TYPE");
		transactiontype.setCreatedAt(new Date());
		log.info("LOG: NEW TRANSACTION TYPE ID: " + transactiontype.getId());
		return transactionTypeRepository.save(transactiontype);
	}
	
	// Edit Transaction Type
	@PutMapping("/{transactionTypeId}")
	public TransactionType updateOneTransactionType(@RequestBody TransactionType transactionTypeUpdates, @PathVariable Long transactionTypeId)
	{
		log.info("LOG: UPDATE TRANSACTION TYPE WITH ID: " + transactionTypeUpdates);
		TransactionType existing = transactionTypeRepository.findById(transactionTypeId).get();
		existing.updateTransactionType(transactionTypeUpdates);
		
		return transactionTypeRepository.save(existing);
	}
	
	// Delete Transaction Type
	@DeleteMapping("/{transactionTypeId}")
	public void deleteOneTransactionType(@PathVariable Long transactionTypeId)
	{
		log.info("LOG: DELETE TRANSACTION TYPE WITH ID: " + transactionTypeId);
		TransactionType typeToDelete = transactionTypeRepository.findById(transactionTypeId).get();
		transactionTypeRepository.delete(typeToDelete);
	}

}
