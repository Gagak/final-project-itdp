package mega.itdp.financialAPI.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mega.itdp.financialAPI.entity.BillTransaction;
import mega.itdp.financialAPI.entity.TransactionCategory;
import mega.itdp.financialAPI.entity.TransactionDetails;
import mega.itdp.financialAPI.entity.TransactionSubCategory;
import mega.itdp.financialAPI.repository.BillTransactionRepository;
import mega.itdp.financialAPI.repository.TransactionCategoryRepository;
import mega.itdp.financialAPI.repository.TransactionDetailsRepository;
import mega.itdp.financialAPI.repository.TransactionSubCategoryRepository;
import mega.itdp.financialAPI.result.ListTransactionDetailsResult;
import mega.itdp.financialAPI.result.TransactionDetailsResult;

@RestController
@RequestMapping(value = "detailTransaksi")
public class TransactionDetailsController
{
	private final Log log = LogFactory.getLog(TransactionDetailsController.class);
	
	@Autowired
	private TransactionDetailsRepository transactionDetailsRepository;
	@Autowired
	private TransactionSubCategoryRepository transactionSubCategoryRepository;
	@Autowired
	private BillTransactionRepository billTransactionRepository;
	@Autowired
	private TransactionCategoryRepository transactionCategoryRepository;
	
	// Get All Transaction Details
	@GetMapping
	public List<TransactionDetailsResult> getAllransactionDetails()
	{
		List<TransactionDetails> selectAll = transactionDetailsRepository.findAll();
		List<TransactionDetailsResult> results = new ArrayList<>();
		
		for (TransactionDetails details : selectAll) {
			results.add(new TransactionDetailsResult(details));
		}
		
		log.info("LOG: GET ALL TRANSACTION DETAILS");
		return results;
	}
	
	// Get All Transaction Details By Bill Id
	@GetMapping("/billId={billId}")
	public List<TransactionDetailsResult> getAllransactionDetailsByBillId(@PathVariable Long billId)
	{
		List<TransactionDetails> selectAll = transactionDetailsRepository.findAllByBillTransactionId(billId);
		List<TransactionDetailsResult> results = new ArrayList<>();
		
		for (TransactionDetails details : selectAll) {
			results.add(new TransactionDetailsResult(details));
		}
		
		log.info("LOG: GET ALL TRANSACTION DETAILS BY BILL ID");
		return results;
	}
	
	// Get All Transaction Details By Sub category
	@GetMapping("/subCategoryId={subCategoryId}")
	public List<TransactionDetailsResult> getAllransactionDetailsByTransactionSubCategoryId(@PathVariable Long subCategoryId)
	{
		List<TransactionDetails> selectAll = transactionDetailsRepository.findAllByTransactionSubCategoryId(subCategoryId);
		List<TransactionDetailsResult> results = new ArrayList<>();
		
		for (TransactionDetails details : selectAll) {
			results.add(new TransactionDetailsResult(details));
		}
		
		log.info("LOG: GET ALL TRANSACTION DETAILS BY TRANSACTION SUB CATEGORY");
		return results;
	}
	
	// Get All Transaction Details By Category
	@GetMapping("/categoryId={categoryId}")
	public List<ListTransactionDetailsResult> getAllransactionDetailsByTransactionCategoryId(@PathVariable Long categoryId)
	{
		List<TransactionDetails> selectAll = transactionDetailsRepository.findAllByCategoryId(categoryId);
		List<ListTransactionDetailsResult> results = new ArrayList<>();
			
		for (TransactionDetails details : selectAll) {
			BillTransaction billTransaction = billTransactionRepository.findById(details.getBillTransaction().getId()).get();
			TransactionCategory transactionCategory = transactionCategoryRepository.findById(categoryId).get();
			
			ListTransactionDetailsResult listTransactionDetailsResult = new ListTransactionDetailsResult();
			listTransactionDetailsResult.setId(details.getId());
			listTransactionDetailsResult.setTransactionDate(billTransaction.getTransactionDate());
			listTransactionDetailsResult.setTotalPrice(details.getPrice());
			listTransactionDetailsResult.setMerchantName(billTransaction.getMerchantName());
			listTransactionDetailsResult.setCategoryName(transactionCategory.getCategoryName());
			listTransactionDetailsResult.setCategoryId(transactionCategory.getCategoryId());
			listTransactionDetailsResult.setCreatedAt(details.getCreatedAt());
			listTransactionDetailsResult.setModifiedAt(details.getModifiedAt());
			
			results.add(listTransactionDetailsResult);
		}
			
		log.info("LOG: GET ALL TRANSACTION DETAILS BY TRANSACTION CATEGORY");
		return results;
	}
	
	// Get One Transaction Details
	@GetMapping("/{detailsId}")
	public TransactionDetailsResult getOneTransactionDetails(@PathVariable Long detailsId)
	{
		TransactionDetails selectOne = transactionDetailsRepository.findById(detailsId).get();
		TransactionDetailsResult result = new TransactionDetailsResult(selectOne);
		
		log.info("LOG: GET ONE TRANSACTION DETAILS");
		return result;
	}
	
	// Add Transaction Details
	@PostMapping
	public TransactionDetailsResult saveOneTransactionDetails(@RequestBody TransactionDetailsResult transactionDetailsResult)
	{
		log.info("LOG: ADD NEW TRANSACTION DETAILS");
		TransactionSubCategory subCategory = null;
		BillTransaction billTransaction = null;
		if (transactionDetailsResult.getTransactionSubCategoryId() != null && transactionDetailsResult.getBillTransactionId() != null) {
			subCategory = transactionSubCategoryRepository.findById(transactionDetailsResult.getTransactionSubCategoryId()).get();
			billTransaction = billTransactionRepository.findById(transactionDetailsResult.getBillTransactionId()).get();
		}
		TransactionDetails transactionDetails = transactionDetailsResult.backToEntity(transactionDetailsResult);
		transactionDetails.setTransactionSubCategory(subCategory);
		transactionDetails.setBillTransaction(billTransaction);
		transactionDetails.setCreatedAt(new Date());
		
		log.info("LOG: NEW TRANSACTION DETAILS ID: " + transactionDetailsResult.getId());
		return new TransactionDetailsResult(transactionDetailsRepository.save(transactionDetails));
	}
	
	// Edit Transaction Details
	@PutMapping("/{detailsId}")
	public TransactionDetailsResult editOneTransactionDetails(@RequestBody TransactionDetailsResult transactionDetailsUpdates, @PathVariable Long detailsId)
	{
		log.info("LOG: UPDATE TRANSACTION DETAILS WITH ID: " + detailsId);
		TransactionSubCategory subCategory = null;
		BillTransaction billTransaction = null;
		if (transactionDetailsUpdates.getTransactionSubCategoryId() != null && transactionDetailsUpdates.getBillTransactionId() != null) {
			subCategory = transactionSubCategoryRepository.findById(transactionDetailsUpdates.getTransactionSubCategoryId()).get();
			billTransaction = billTransactionRepository.findById(transactionDetailsUpdates.getBillTransactionId()).get();
		}
		TransactionDetails existing = transactionDetailsRepository.findById(detailsId).get();
		
		existing.setVersion(transactionDetailsUpdates.getVersion());
		existing.updateFields(transactionDetailsUpdates);
		existing.setModifiedAt(new Date());
		existing.setTransactionSubCategory(subCategory);
		existing.setBillTransaction(billTransaction);
		
		return new TransactionDetailsResult(transactionDetailsRepository.save(existing));
	}
	
	// Deletes Transaction Details
	@DeleteMapping("/{detailId}")
	public void deleteOneTransactionSubCategory(@PathVariable Long detailId)
	{
		TransactionDetails toDelete = transactionDetailsRepository.findById(detailId).get();
		transactionDetailsRepository.delete(toDelete);
		log.info("LOG: DELETE TRANSACTION DETAILS WITH ID: " + detailId);
	}
}
