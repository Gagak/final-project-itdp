package mega.itdp.financialAPI.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mega.itdp.financialAPI.entity.BillTransaction;
import mega.itdp.financialAPI.entity.TransactionCategory;
import mega.itdp.financialAPI.entity.TransactionDetails;
import mega.itdp.financialAPI.repository.BillTransactionRepository;
import mega.itdp.financialAPI.repository.TransactionCategoryRepository;
import mega.itdp.financialAPI.repository.TransactionDetailsRepository;
import mega.itdp.financialAPI.result.ListTransactionDetailsResult;

@RestController
@RequestMapping(value = "transaksi-detail-by-date")
public class TransactionDetailsByDateController
{
	private final Log log = LogFactory.getLog(TransactionDetailsByDateController.class);
	
	@Autowired
	private TransactionDetailsRepository transactionDetailsRepository;
	@Autowired
	private BillTransactionRepository billTransactionRepository;
	@Autowired
	private TransactionCategoryRepository transactionCategoryRepository;
	
	// Get All Transaction Details By Date Range
	@GetMapping("/startDate={startDate}&endDate={endDate}")
	public List<ListTransactionDetailsResult> getAllTransactionDetailsByDateRange(@PathVariable String startDate, @PathVariable String endDate)
	{
		log.info("LOG: GET ALL TRANSACTION DETAILS BETWEEN: " + startDate + " AND " + endDate);
		List<TransactionDetails> selectAll;
		try {
			selectAll = transactionDetailsRepository.findAllByDateRange(new SimpleDateFormat("yyyy-MM-dd").parse(startDate), new SimpleDateFormat("yyyy-MM-dd").parse(endDate));
		
			List<ListTransactionDetailsResult> results = new ArrayList<>();
			
			for (TransactionDetails details : selectAll) {
				BillTransaction billTransaction = billTransactionRepository.findById(details.getBillTransaction().getId()).get();
				TransactionCategory transactionCategory = transactionCategoryRepository.findById(details.getTransactionSubCategory().getTransactionCategory().getId()).get();
				
				ListTransactionDetailsResult listTransactionDetailsResult = new ListTransactionDetailsResult();
				listTransactionDetailsResult.setId(details.getId());
				listTransactionDetailsResult.setTransactionDate(billTransaction.getTransactionDate());
				listTransactionDetailsResult.setTotalPrice(details.getPrice());
				listTransactionDetailsResult.setMerchantName(billTransaction.getMerchantName());
				listTransactionDetailsResult.setCategoryName(transactionCategory.getCategoryName());
				listTransactionDetailsResult.setCategoryId(transactionCategory.getCategoryId());
				listTransactionDetailsResult.setCreatedAt(details.getCreatedAt());
				listTransactionDetailsResult.setModifiedAt(details.getModifiedAt());
				
				results.add(listTransactionDetailsResult);
			}
			
			return results;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	@GetMapping("/startDate={startDate}&endDate={endDate}&categoryId={categoryId}")
	public List<ListTransactionDetailsResult> getAllTransactionDetailsByDateRangeAndCategoryId(@PathVariable String startDate, @PathVariable String endDate, @PathVariable Long categoryId)
	{
		log.info("LOG: GET ALL TRANSACTION DETAILS BETWEEN ");
		List<TransactionDetails> selectAll;
		try {
			selectAll = transactionDetailsRepository.findAllByDateRangeAndCategoryId(new SimpleDateFormat("yyyy-MM-dd").parse(startDate), new SimpleDateFormat("yyyy-MM-dd").parse(endDate), categoryId);
		
		List<ListTransactionDetailsResult> results = new ArrayList<>();
		
		for (TransactionDetails details : selectAll) {
			BillTransaction billTransaction = billTransactionRepository.findById(details.getBillTransaction().getId()).get();
			TransactionCategory transactionCategory = transactionCategoryRepository.findById(categoryId).get();
			
			ListTransactionDetailsResult listTransactionDetailsResult = new ListTransactionDetailsResult();
			listTransactionDetailsResult.setId(details.getId());
			listTransactionDetailsResult.setTransactionDate(billTransaction.getTransactionDate());
			listTransactionDetailsResult.setTotalPrice(details.getPrice());
			listTransactionDetailsResult.setMerchantName(billTransaction.getMerchantName());
			listTransactionDetailsResult.setCategoryName(transactionCategory.getCategoryName());
			listTransactionDetailsResult.setCategoryId(transactionCategory.getCategoryId());
			listTransactionDetailsResult.setCreatedAt(details.getCreatedAt());
			listTransactionDetailsResult.setModifiedAt(details.getModifiedAt());
			
			results.add(listTransactionDetailsResult);
		}
		
		return results;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}