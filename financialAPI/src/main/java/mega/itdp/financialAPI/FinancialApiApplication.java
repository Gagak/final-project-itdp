package mega.itdp.financialAPI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class FinancialApiApplication extends SpringBootServletInitializer{
	
	public static void main(String[] args) {
		SpringApplication.run(FinancialApiApplication.class, args);
	}

}
