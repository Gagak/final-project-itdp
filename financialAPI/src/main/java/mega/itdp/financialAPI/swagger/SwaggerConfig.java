package mega.itdp.financialAPI.swagger;

import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	@Bean
	public Docket api()
	{
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("mega.itdp.financialAPI.controllers"))
				.paths(PathSelectors.any())
				.build()
				.apiInfo(apiInfo());
	}
	
	private ApiInfo apiInfo()
	{
		ApiInfo apiInfo = new ApiInfo(
			"Financial Management API",
			"This is the documentation for all endpoints/link to get the data from database to build the financial management feature.",
			"Version 1.0.0",
			"ITDP MEGA Terms of Service",
			new Contact("Duan Je Martiko Manurung", "www.bankmega.com", "crlraditz@gmail.com"),
			"Apache License",
			"www.apache.com",
			Collections.emptyList()
		);
		return apiInfo;
	}
}
