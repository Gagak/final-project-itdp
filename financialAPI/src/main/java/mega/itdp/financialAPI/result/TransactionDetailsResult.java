package mega.itdp.financialAPI.result;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import mega.itdp.financialAPI.entity.TransactionDetails;

public class TransactionDetailsResult
{
	private Long id;
	private Long version;
	private String itemName;
	private int itemQuantity;
	private Double price;
	private Date createdAt;
	private Date modifiedAt;
	
	@JsonProperty(value = "bill_transaction_id")
	private Long billTransactionId;
	
	@JsonProperty(value = "transaction_subcategory_id")
	private Long transactionSubCategoryId;
	
	public TransactionDetailsResult() {}
	
	public TransactionDetailsResult(TransactionDetails transactionDetails)
	{
		this.id = transactionDetails.getId();
		this.version = transactionDetails.getVersion();
		this.itemName = transactionDetails.getItemName();
		this.itemQuantity = transactionDetails.getItemQuantity();
		this.price = transactionDetails.getPrice();
		this.billTransactionId = transactionDetails.getBillTransaction().getId();
		this.transactionSubCategoryId = transactionDetails.getTransactionSubCategory().getId();
		this.createdAt = transactionDetails.getCreatedAt();
		this.modifiedAt = transactionDetails.getModifiedAt();
	}
	
	public TransactionDetails backToEntity(TransactionDetailsResult transactionDetailsResult)
	{
		TransactionDetails transactionDetails = new TransactionDetails();
		
		transactionDetails.setId(transactionDetailsResult.getId());
		transactionDetails.setVersion(transactionDetailsResult.getVersion());
		transactionDetails.setItemName(transactionDetailsResult.getItemName());
		transactionDetails.setItemQuantity(transactionDetailsResult.getItemQuantity());
		transactionDetails.setPrice(transactionDetailsResult.getPrice());
		transactionDetails.setCreatedAt(transactionDetailsResult.getCreatedAt());
		transactionDetails.setModifiedAt(transactionDetailsResult.getModifiedAt());
		
		return transactionDetails;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public int getItemQuantity() {
		return itemQuantity;
	}

	public void setItemQuantity(int itemQuantity) {
		this.itemQuantity = itemQuantity;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getModifiedAt() {
		return modifiedAt;
	}

	public void setModifiedAt(Date modifiedAt) {
		this.modifiedAt = modifiedAt;
	}

	public Long getBillTransactionId() {
		return billTransactionId;
	}

	public void setBillTransactionId(Long billTransactionId) {
		this.billTransactionId = billTransactionId;
	}

	public Long getTransactionSubCategoryId() {
		return transactionSubCategoryId;
	}

	public void setTransactionSubCategoryId(Long transactionSubCategoryId) {
		this.transactionSubCategoryId = transactionSubCategoryId;
	}
}
