package mega.itdp.financialAPI.result;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import mega.itdp.financialAPI.entity.BillTransaction;

public class ListBillTransactionResult
{
	private Long id;
	private Long version;
	private Date transactionDate;
	private String merchantName;
	private Double totalPrice;
	private int totalItem;
	private int totalCategory;
	private Date createdAt;
	private Date modifiedAt;
	
	@JsonProperty(value="customer_id")
	private Long customerId;
	
	@JsonProperty(value="transaction_type_id")
	private Long transactionTypeId;
	
	public ListBillTransactionResult(){}
	
	public ListBillTransactionResult(BillTransaction billTransaction)
	{
		this.id = billTransaction.getId();
		this.version = billTransaction.getVersion();
		this.transactionDate = billTransaction.getTransactionDate();
		this.merchantName = billTransaction.getMerchantName();
		this.totalPrice = billTransaction.getTotalPrice();
		this.totalItem = 0;
		this.totalCategory = 0;
		this.customerId = billTransaction.getCustomer().getId();
		this.transactionTypeId = billTransaction.getTransactionType().getId();
		this.createdAt = billTransaction.getCreatedAt();
		this.modifiedAt = billTransaction.getModifiedAt();
	}
	
	public BillTransaction backToEntity(ListBillTransactionResult listBillTransactionResult)
	{
		BillTransaction billTransaction = new BillTransaction();
		
		billTransaction.setId(listBillTransactionResult.getId());
		billTransaction.setVersion(listBillTransactionResult.getVersion());
		billTransaction.setTransactionDate(listBillTransactionResult.getTransactionDate());
		billTransaction.setMerchantName(listBillTransactionResult.getMerchantName());
		billTransaction.setTotalPrice(listBillTransactionResult.getTotalPrice());
		billTransaction.setCreatedAt(listBillTransactionResult.getCreatedAt());
		billTransaction.setModifiedAt(listBillTransactionResult.getModifiedAt());
		
		return billTransaction;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public Double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public int getTotalItem() {
		return totalItem;
	}

	public void setTotalItem(int totalItem) {
		this.totalItem = totalItem;
	}

	public int getTotalCategory() {
		return totalCategory;
	}

	public void setTotalCategory(int totalCategory) {
		this.totalCategory = totalCategory;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getModifiedAt() {
		return modifiedAt;
	}

	public void setModifiedAt(Date modifiedAt) {
		this.modifiedAt = modifiedAt;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Long getTransactionTypeId() {
		return transactionTypeId;
	}

	public void setTransactionTypeId(Long transactionTypeId) {
		this.transactionTypeId = transactionTypeId;
	}
}
