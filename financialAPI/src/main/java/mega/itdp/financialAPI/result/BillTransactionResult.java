package mega.itdp.financialAPI.result;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import mega.itdp.financialAPI.entity.BillTransaction;

public class BillTransactionResult
{
	private Long id;
	private Long version;
	private Date transactionDate;
	private String merchantName;
	private Double totalPrice;
	private Date createdAt;
	private Date modifiedAt;
	
	@JsonProperty(value="customer_id")
	private Long customerId;
	
	@JsonProperty(value="transaction_type_id")
	private Long transactionTypeId;
	
	public BillTransactionResult() {}
	
	public BillTransactionResult(BillTransaction billTransaction)
	{
		this.id = billTransaction.getId();
		this.version = billTransaction.getVersion();
		this.transactionDate = billTransaction.getTransactionDate();
		this.merchantName = billTransaction.getMerchantName();
		this.totalPrice = billTransaction.getTotalPrice();
		this.customerId = billTransaction.getCustomer().getId();
		this.transactionTypeId = billTransaction.getTransactionType().getId();
		this.createdAt = billTransaction.getCreatedAt();
		this.modifiedAt = billTransaction.getModifiedAt();
	}
	
	public BillTransaction backToEntity(BillTransactionResult billTransactionResult)
	{
		BillTransaction billTransaction = new BillTransaction();
		
		billTransaction.setId(billTransactionResult.getId());
		billTransaction.setVersion(billTransactionResult.getVersion());
		billTransaction.setTransactionDate(billTransactionResult.getTransactionDate());
		billTransaction.setMerchantName(billTransactionResult.getMerchantName());
		billTransaction.setTotalPrice(billTransactionResult.getTotalPrice());
		billTransaction.setCreatedAt(billTransactionResult.getCreatedAt());
		billTransaction.setModifiedAt(billTransactionResult.getModifiedAt());
		
		return billTransaction;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public Double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getModifiedAt() {
		return modifiedAt;
	}

	public void setModifiedAt(Date modifiedAt) {
		this.modifiedAt = modifiedAt;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Long getTransactionTypeId() {
		return transactionTypeId;
	}

	public void setTransactionTypeId(Long transactionTypeId) {
		this.transactionTypeId = transactionTypeId;
	}
}
