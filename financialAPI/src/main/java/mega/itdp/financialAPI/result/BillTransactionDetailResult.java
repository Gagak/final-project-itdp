package mega.itdp.financialAPI.result;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import mega.itdp.financialAPI.entity.BillTransaction;
import mega.itdp.financialAPI.entity.TransactionCategory;
import mega.itdp.financialAPI.entity.TransactionDetails;

public class BillTransactionDetailResult
{
	private Long id;
	private Long version;
	private Date transactionDate;
	private String merchantName;
	private Double totalPrice;
	private Date createdAt;
	private Date modifiedAt;
	
	@JsonProperty(value="customer_id")
	private Long customerId;
	
	@JsonProperty(value="transaction_type_id")
	private Long transactionTypeId;
	
	@JsonProperty(value="category_by_bill")
	private List<TransactionCategory> categoryByBill;
	
	@JsonProperty(value="details_by_category_in_bill")
	private List<List<TransactionDetails>> details;
	
	public BillTransactionDetailResult() {}
	
	public BillTransactionDetailResult(BillTransaction billTransaction) 
	{
		this.id = billTransaction.getId();
		this.version = billTransaction.getVersion();
		this.transactionDate = billTransaction.getTransactionDate();
		this.merchantName = billTransaction.getMerchantName();
		this.totalPrice = billTransaction.getTotalPrice();
		this.customerId = billTransaction.getCustomer().getId();
		this.transactionTypeId = billTransaction.getTransactionType().getId();
		this.createdAt = billTransaction.getCreatedAt();
		this.modifiedAt = billTransaction.getModifiedAt();
	}
	
	public BillTransaction backToEntity(BillTransactionDetailResult billTransactionDetailResult)
	{
		BillTransaction billTransaction = new BillTransaction();
		
		billTransaction.setId(billTransactionDetailResult.getId());
		billTransaction.setVersion(billTransactionDetailResult.getVersion());
		billTransaction.setTransactionDate(billTransactionDetailResult.getTransactionDate());
		billTransaction.setMerchantName(billTransactionDetailResult.getMerchantName());
		billTransaction.setTotalPrice(billTransactionDetailResult.getTotalPrice());
		billTransaction.setCreatedAt(billTransactionDetailResult.getCreatedAt());
		billTransaction.setModifiedAt(billTransactionDetailResult.getModifiedAt());
		
		return billTransaction;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public Double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getModifiedAt() {
		return modifiedAt;
	}

	public void setModifiedAt(Date modifiedAt) {
		this.modifiedAt = modifiedAt;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Long getTransactionTypeId() {
		return transactionTypeId;
	}

	public void setTransactionTypeId(Long transactionTypeId) {
		this.transactionTypeId = transactionTypeId;
	}

	public List<TransactionCategory> getCategoryByBill() {
		return categoryByBill;
	}

	public void setCategoryByBill(List<TransactionCategory> categoryByBill) {
		this.categoryByBill = categoryByBill;
	}

	public List<List<TransactionDetails>> getDetails() {
		return details;
	}

	public void setDetails(List<List<TransactionDetails>> details) {
		this.details = details;
	}
}
