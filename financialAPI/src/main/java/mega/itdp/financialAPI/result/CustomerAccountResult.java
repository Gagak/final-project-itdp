package mega.itdp.financialAPI.result;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import mega.itdp.financialAPI.entity.CustomerAccount;

public class CustomerAccountResult
{
	private Long id;
	private Long version;
	private Double accountBalance;
	private Date createdAt;
	private Date modifiedAt;
	
	@JsonProperty(value="customer_id")
	private Long customerId;
	
	public CustomerAccountResult() {}
	
	public CustomerAccountResult(CustomerAccount customerAccount)
	{
		this.id = customerAccount.getId();
		this.version = customerAccount.getVersion();
		this.accountBalance = customerAccount.getAccountBalance();
		this.customerId = customerAccount.getCustomer().getId();
		this.createdAt = customerAccount.getCreatedAt();
		this.modifiedAt = customerAccount.getModifiedAt();
	}
	
	public CustomerAccount backToEntity(CustomerAccountResult customerAccountResult)
	{
		CustomerAccount customerAccount = new CustomerAccount();
		
		customerAccount.setId(customerAccountResult.getId());
		customerAccount.setVersion(customerAccountResult.getVersion());
		customerAccount.setAccountBalance(customerAccountResult.getAccountBalance());
		customerAccount.setCreatedAt(customerAccountResult.getCreatedAt());
		customerAccount.setModifiedAt(customerAccountResult.getModifiedAt());
		
		return customerAccount;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Double getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(Double accountBalance) {
		this.accountBalance = accountBalance;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getModifiedAt() {
		return modifiedAt;
	}

	public void setModifiedAt(Date modifiedAt) {
		this.modifiedAt = modifiedAt;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
}
