package mega.itdp.financialAPI.result;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import mega.itdp.financialAPI.entity.TransactionSubCategory;

public class TransactionSubCategoryResult
{
	private Long id;
	private Long version;
	private String subCategoryId;
	private String subCategoryName;
	private Date createdAt;
	private Date modifiedAt;
	
	@JsonProperty(value="transaction_category_id")
	private Long transactionCategoryId;
	
	public TransactionSubCategoryResult() {}
	
	public TransactionSubCategoryResult(TransactionSubCategory transactionSubCategory)
	{
		this.id = transactionSubCategory.getId();
		this.version = transactionSubCategory.getVersion();
		this.subCategoryId = transactionSubCategory.getSubCategoryId();
		this.subCategoryName = transactionSubCategory.getSubCategoryName();
		this.transactionCategoryId = transactionSubCategory.getTransactionCategory().getId();
		this.createdAt = transactionSubCategory.getCreatedAt();
		this.modifiedAt = transactionSubCategory.getModifiedAt();
	}
	
	public TransactionSubCategory backToEntity(TransactionSubCategoryResult transactionSubCategoryResult)
	{
		TransactionSubCategory transactionSubCategory = new TransactionSubCategory();
		
		transactionSubCategory.setId(transactionSubCategoryResult.getId());
		transactionSubCategory.setVersion(transactionSubCategoryResult.getVersion());
		transactionSubCategory.setSubCategoryId(transactionSubCategoryResult.getSubCategoryId());
		transactionSubCategory.setSubCategoryName(transactionSubCategoryResult.getSubCategoryName());
		transactionSubCategory.setCreatedAt(transactionSubCategoryResult.getCreatedAt());
		transactionSubCategory.setModifiedAt(transactionSubCategoryResult.getModifiedAt());
		
		return transactionSubCategory;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getSubCategoryId() {
		return subCategoryId;
	}

	public void setSubCategoryId(String subCategoryId) {
		this.subCategoryId = subCategoryId;
	}

	public String getSubCategoryName() {
		return subCategoryName;
	}

	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getModifiedAt() {
		return modifiedAt;
	}

	public void setModifiedAt(Date modifiedAt) {
		this.modifiedAt = modifiedAt;
	}

	public Long getTransactionCategoryId() {
		return transactionCategoryId;
	}

	public void setTransactionCategoryId(Long transactionCategoryId) {
		this.transactionCategoryId = transactionCategoryId;
	}
}
